package ba.unsa.etf.rma.aktivnosti;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;
import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.AlertDijalog;
import ba.unsa.etf.rma.klase.DohvatiIzBaze;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.MyAsyncTask;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.helper;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorijeIzBaze;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorijeSQL;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.konektovanoNaInternet;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kvizoviBaza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.pitanjaIzBaze;

public class DodajKategorijuAkt extends AppCompatActivity implements IconDialog.Callback{
    EditText naziv;
    EditText ikona;
    Button dodajIkonu;
    Button dodajKategoriju;
    private Icon[] selectedIcons;
    IconDialog iconDialog = new IconDialog();
    private Kategorija kategorija;
    public static boolean dodajKAtegoriju=false ;

    public class UcitajKategorijeIzBaze extends AsyncTask<String, Void, String> {
        //ova klasa ucitava iz baze u aplikaciju

        DohvatiIzBaze interfejs = null;

        public UcitajKategorijeIzBaze(DohvatiIzBaze interfejs) {

            this.interfejs = interfejs;

        }

        /* @Override
         protected void onPreExecute() {
             this.dialog.setMessage("Dohvatanje podataka iz baze...");
             this.dialog.show();
         }*/
        @Override
        protected String doInBackground(String... params) {
            // boolean zavrseno = false;
            //ucitava sve sto se nalazi u bazi

            InputStream is = getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));

                credentials.refreshToken();

                String TOKEN = credentials.getAccessToken();
                System.out.println(TOKEN);


                String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/";
                if(!kategorijeIzBaze.isEmpty() || !kvizoviBaza.isEmpty() || !pitanjaIzBaze.isEmpty()){
                    kategorijeIzBaze.clear();
                    kvizoviBaza.clear();
                    pitanjaIzBaze.clear();
                }

                //Za ucitavanje KATEGORIJA iz baze
                String urlKategorije = url + "Kategorije?access_token=";
                URL urlObject1 = new URL(urlKategorije + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn1 = (HttpURLConnection) urlObject1.openConnection();
                conn1.setRequestProperty("Authorization", "Bearer"+TOKEN);
                InputStream odgovor1 = new BufferedInputStream(conn1.getInputStream());

                try {
                    String rezultat1 = convertStreamToString(odgovor1);
                    JSONObject jo = new JSONObject(rezultat1);
                    JSONArray kategorije = jo.getJSONArray("documents");
                    for(int i=0;i<kategorije.length();i++){
                        JSONObject kategorija = kategorije.getJSONObject(i);
                        JSONObject atributi = kategorija.getJSONObject("fields");
                        JSONObject naziv = atributi.getJSONObject("naziv");
                        String nazivKategorije = naziv.getString("stringValue");
                        JSONObject id = atributi.getJSONObject("idIkonice");
                        int idIkone = id.getInt("integerValue");
                        //System.out.println(idIkone);
                        Kategorija nova = new Kategorija(nazivKategorije, String.valueOf(idIkone));
                        kategorijeIzBaze.add(nova);
                    }


                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }catch(IOException e){

            }

            return "";
        }
        public String convertStreamToString(InputStream is){
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            try{
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }
        @Override
        protected void onPostExecute(String result) {
            //if (this.dialog.isShowing()) { // if dialog box showing = true
            //  this.dialog.dismiss(); // dismiss it
            //}
            interfejs.krajUcitavanja(result);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodajkategoriju_akt);
        kategorija =new Kategorija();
        //kat = new Kategorija();

        konektovanoNaInternet = konektovanoNaInternet();

        if(konektovanoNaInternet) {
            new UcitajKategorijeIzBaze(new DohvatiIzBaze() {

                @Override
                public void krajUcitavanja(String output) {


                }
            }).execute(" ");
        }else{
            //ucitaj iz sql
            kategorijeSQL = helper.dohvatiKategorijeIzSQL();
        }

        if(DodajKvizAkt.imeKategorije.equals("Dodaj Kategoriju")) {
            konektovanoNaInternet = konektovanoNaInternet();

            naziv = (EditText) findViewById(R.id.etNaziv);
            ikona = (EditText) findViewById(R.id.etIkona);
            //ikona.setEnabled(false);
            dodajIkonu = (Button) findViewById(R.id.btnDodajIkonu);
            dodajKategoriju = (Button) findViewById(R.id.btnDodajKategoriju);


            dodajIkonu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iconDialog.setMaxSelection(1, true);
                    iconDialog.setSelectedIcons(selectedIcons);
                    iconDialog.show(getSupportFragmentManager(), "icon_dialog");
                }
            });
            if (konektovanoNaInternet) {
                dodajKategoriju.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean validno = validacija();
                        if (validno && !ikona.getText().toString().equals("")) {
                            kategorija.setNaziv(naziv.getText().toString());
                            kategorija.setId(ikona.getText().toString());

                            //if(!postojiKategorija(naziv.getText().toString())) {
                            // KvizoviAkt.temp.add(kategorija);
                            //}

                            dodajKAtegoriju = true;
                            Intent intent = getIntent();
                            Bundle naziv = new Bundle();
                            naziv.putSerializable("kategorija", kategorija);
                            intent.putExtras(naziv);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                });
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(DodajKategorijuAkt.this);
                builder.setTitle("Upozorenje!");
                builder.setMessage("Nemate pristup internetu!");
                builder.setNegativeButton("OK", null);
                AlertDialog dijalog = builder.create();
                dijalog.show();
            }
        }
    }



    @Override
    public void onBackPressed(){
        Intent vrati = new Intent(getApplicationContext(), DodajKvizAkt.class);
        vrati.putExtra("ime", 5);
        setResult(RESULT_OK, vrati);
        finish();
    }

    @Override
    public void onIconDialogIconsSelected(Icon[] icons){
        selectedIcons = icons;
        if(icons.length!=0) {
            ikona.setText(Integer.toString(icons[0].getId()));
        }
    }

    private boolean validacija(){
        boolean validno = true;
        if(naziv.getText().toString().equals("")){
            validno = false;
            naziv.setBackgroundColor(getResources().getColor(R.color.netacno));
        }else{
            if(konektovanoNaInternet) {
                for (Kategorija k : kategorijeIzBaze) {
                    if (k.getNaziv().equals(naziv.getText().toString())) {
                        validno = false;
                        DodajKvizAkt.porukaDijaloga = "Unesena kategorija vec postoji!";
                        openDialog();
                        break;
                    }
                }
            }else{
                for (Kategorija k : kategorijeSQL) {
                    if (k.getNaziv().equals(naziv.getText().toString())) {
                        validno = false;
                        DodajKvizAkt.porukaDijaloga = "Unesena kategorija vec postoji!";
                        openDialog();
                        break;
                    }
                }
            }
        }
        return validno;
    }

    private boolean postojiKategorija(String s){
        boolean postoji=false;
        for(Kategorija k: KvizoviAkt.temp){
            if(k.getNaziv().equals(s)){
                postoji=true;
                break;
            }
        }
        return postoji;
    }
    public void openDialog(){
        AlertDijalog exampleDialog = new AlertDijalog();
        exampleDialog.show(getSupportFragmentManager(), "example");
    }

    public boolean konektovanoNaInternet() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else
            connected = false;

        return connected;
    }
}
