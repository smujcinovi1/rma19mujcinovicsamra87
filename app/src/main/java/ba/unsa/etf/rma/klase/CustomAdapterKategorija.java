package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class CustomAdapterKategorija extends ArrayAdapter<Kategorija> {

        // Your sent context
        private Context context;
        // Your custom values for the spinner (User)
        private ArrayList<Kategorija> kat;

        public CustomAdapterKategorija(Context context, int textViewResourceId, ArrayList<Kategorija> val) {
            super(context, textViewResourceId, val);
            this.context = context;
            this.kat = val;
        }

        @Override
        public int getCount(){
            return kat.size();
        }

        @Override
        public Kategorija getItem(int position){
            return kat.get(position);
        }

        @Override
        public long getItemId(int position){
            return position;
        }

    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = (TextView) super.getView(position, convertView, parent);

        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(kat.get(position).getNaziv());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setText(kat.get(position).getNaziv());

        return label;
    }
}
