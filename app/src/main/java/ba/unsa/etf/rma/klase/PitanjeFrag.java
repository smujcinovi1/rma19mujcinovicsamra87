package ba.unsa.etf.rma.klase;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;

public class PitanjeFrag extends Fragment {

    TextView nazivPitanja;
    ListView listaOdgovoraPitanja;
    Pitanje pitanje = new Pitanje();
    ArrayList<String> odgovori = new ArrayList<>();
    ArrayAdapter<String> adapterOdgovor;
    boolean tacnoOdgovorio;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle){
        //ovdje se dodjeljuje layout fragmentu, tj.sta ce se nalazitiunutar fragmenta
        View v = inflater.inflate(R.layout.fragment_pitanje, container, false);
        return  v;
    }

    @Override
    public void onActivityCreated(Bundle bundle){
        super.onActivityCreated(bundle);

        final IgrajKvizAkt aktivnost = (IgrajKvizAkt) getActivity();

        if(getArguments().containsKey("pitanje")){
            pitanje = (Pitanje) getArguments().getSerializable("pitanje");
        }

                nazivPitanja = (TextView) getView().findViewById(R.id.tekstPitanja);
                listaOdgovoraPitanja = (ListView) getView().findViewById(R.id.odgovoriPitanja);

                nazivPitanja.setText(pitanje.getNaziv());
                odgovori = pitanje.getOdgovori();

                adapterOdgovor = new ArrayAdapter<String>(getView().getContext(), android.R.layout.simple_list_item_1, odgovori);

                listaOdgovoraPitanja.setAdapter(adapterOdgovor);

                listaOdgovoraPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (pitanje.getTacan().equals(odgovori.get(position))) {
                            parent.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.tacanOdg));
                            tacnoOdgovorio = true;
                        } else {
                            parent.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.netacanOdg));
                            //trazimo tacan odgovor u lisi odgovora da bismo ga mogli obojiti u zeleno
                            int pozicija = nadjiIndexTacnog(pitanje.getTacan());
                            parent.getChildAt(pozicija).setBackgroundColor(getResources().getColor(R.color.tacanOdg));
                            tacnoOdgovorio = false;
                        }
                            aktivnost.metodaKojomSeVracaUAktivnost(tacnoOdgovorio);

                    }
                });


    }

    private int nadjiIndexTacnog(String s){
        int index = odgovori.indexOf(s);
        return index;
    }

}
