package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconHelper;
import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class CustomAdapterKviz extends BaseAdapter {
    private Activity aktivnost;
    private ArrayList lista;
    private static LayoutInflater inflater=null;
    public Resources res;
    Kviz kviz=null;
    int i=0;

    public CustomAdapterKviz(Activity aktivnost, ArrayList lista, Resources res) {
        this.aktivnost = aktivnost;
        this.lista = lista;
        this.res = res;

        inflater = (LayoutInflater) aktivnost.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount(){
        if(lista.size()<=0)
            return 1;
        return lista.size();
    }

    @Override
    public Object getItem(int position){
        return position;
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    public static class ViewHolder{
        //ono sto ce se prikazivati u listi
        public TextView naziv;
        public ImageView ikona;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        //ViewHolder holder;
        final CustomAdapterKviz.ViewHolder holder;

        if(convertView==null){
            vi=inflater.inflate(R.layout.element_liste_kviz,null);
            holder=new CustomAdapterKviz.ViewHolder();
            holder.naziv = (TextView) vi.findViewById(R.id.naziv);
            holder.ikona = (ImageView) vi.findViewById(R.id.slicica);

            vi.setTag(holder);

        }else {
            holder = (CustomAdapterKviz.ViewHolder) vi.getTag();
        }if(lista.size()>0) {
            kviz = null;
            kviz = (Kviz) lista.get(position);
            holder.naziv.setText(kviz.getNaziv());
            if (kviz.getKategorija() != null) {
                if (kviz.getKategorija().getId() != null) {
                    //final ImageView ikonaKat = convertView.findViewById(R.id.slicica);
                    Icon ikona;
                    final int id = Integer.parseInt(kviz.getKategorija().getId());
                   /* ikona = IconHelper.getInstance(aktivnost).getIcon(id);
                    Drawable drawable = ikona.getDrawable(aktivnost);
                    ikonaKat.setImageDrawable(drawable);*/
                   final IconHelper iconHelper= IconHelper.getInstance(aktivnost);
                   iconHelper.addLoadCallback(new IconHelper.LoadCallback() {
                       @Override
                       public void onDataLoaded() {
                           holder.ikona.setImageDrawable(iconHelper.getIcon(id).getDrawable(aktivnost));
                       }
                   });
                } else {
                    final ImageView ikonaKat = convertView.findViewById(R.id.slicica);
                    Icon ikona;
                    int id = 10;
                    ikona = IconHelper.getInstance(aktivnost).getIcon(id);
                    Drawable drawable = ikona.getDrawable(aktivnost);
                    ikonaKat.setImageDrawable(drawable);
                }

            }
        }


        return  vi;
    }
}
