package ba.unsa.etf.rma.klase;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

public class ListaFrag extends Fragment {

    ListView listaKat;
    public static ArrayList<Kategorija> kategorije ;
    public static CustomAdapterKategorija adapter;
    //Kategorija k ;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lista, container, false);
        listaKat = (ListView) view.findViewById(R.id.listaKategorija);
        return view;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        kategorije = new ArrayList<>(KvizoviAkt.temp);
        kategorije.add(0, new Kategorija("Svi", null));

        adapter = new CustomAdapterKategorija(getContext(), android.R.layout.simple_list_item_1, kategorije);
        listaKat.setAdapter(adapter);


        listaKat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DetailFrag.kvizovi.clear();
                for (Kviz kviz : KvizoviAkt.kvizoviTemp) {
                    if (kviz.getKategorija().getNaziv().equals(kategorije.get(position).getNaziv()) || kategorije.get(position).getNaziv().equals("Svi")) {
                        DetailFrag.kvizovi.add(kviz);
                    }
                }
                DetailFrag.kvizovi.add(new Kviz("Dodaj Kviz", null, null));
                DetailFrag.adapter.notifyDataSetChanged();

            }
        });
    }

}
