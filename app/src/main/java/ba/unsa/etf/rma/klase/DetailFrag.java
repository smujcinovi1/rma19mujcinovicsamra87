package ba.unsa.etf.rma.klase;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.DodajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

import static ba.unsa.etf.rma.aktivnosti.DodajKvizAkt.listaPitanja;

public class DetailFrag extends Fragment {

    GridView grid;
    public static ArrayList<Kviz> kvizovi ;
    public static CustomAdapterGridView adapter ;
    public static int poz;
    Kviz pocetni ;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detalji, container, false);
        grid  = (GridView) view.findViewById(R.id.gridKvizovi);
        return view;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //if(pocetni != null)
          //  pocetni = (Kviz) getArguments().getSerializable("proslijedii");

        kvizovi = new ArrayList<>(KvizoviAkt.kvizoviTemp);
        //kvizovi.add(pocetni);
        kvizovi.add( new Kviz("Dodaj Kviz", null, null));

        adapter = new CustomAdapterGridView(getContext(), kvizovi);
        grid.setAdapter(adapter);

        grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == kvizovi.size()-1) {
                    Intent intent = new Intent(getActivity(), DodajKvizAkt.class);
                    startActivityForResult(intent, 100);
                }else{
                    poz = position;
                    Intent intent = new Intent(getActivity(), DodajKvizAkt.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("kviz", kvizovi.get(position));
                    intent.putExtras(bundle);
                    startActivityForResult(intent,101);

                }

                return true;
            }
        });

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != kvizovi.size() - 1) {
                    Intent intent = new Intent(getContext(), IgrajKvizAkt.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("proslijedi", kvizovi.get(position));
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });


    }



}
