package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.AlertDijalog;
import ba.unsa.etf.rma.klase.DohvatiIzBaze;
import ba.unsa.etf.rma.klase.Pitanje;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.helper;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.konektovanoNaInternet;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.pitanjaSQL;

public class DodajPitanjeAkt extends AppCompatActivity {
    private ArrayList<String> listaOdgovora = new ArrayList<>();

    EditText nazivPitanja;
    EditText textOdgovora;
    ListView listaOdg;
    Button btnDodaj;
    Button btnDodajTacan;
    Button btnSpasi;
    ArrayAdapter<String> adapterOdgovor;
    Pitanje pitanje ;
    boolean tacno = false;
    public static boolean dodajPitanje = false;
    public static boolean dodajPitanjeIzMogucih = false;

    public class UcitajPitanjaIzBaze extends AsyncTask<String, Void, String> {

        public UcitajPitanjaIzBaze(DohvatiIzBaze interfejs) {
            //this.aktivnost = aktivnost;
            this.interfejs = interfejs;
        }

        DohvatiIzBaze interfejs = null;
        //private Activity aktivnost;



        @Override
        protected String doInBackground(String... params) {
            InputStream is = getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));

                credentials.refreshToken();

                String TOKEN = credentials.getAccessToken();
                System.out.println(TOKEN);

                if(!KvizoviAkt.pitanjaIzBaze.isEmpty()) {
                    KvizoviAkt.pitanjaIzBaze.clear();
                }

                String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/";
                //Za ucitavanje PITANJA iz baze
                String urlPitanja = url + "Pitanja?access_token=";
                URL urlObject2 = new URL(urlPitanja + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn2 = (HttpURLConnection) urlObject2.openConnection();
                conn2.setRequestProperty("Authorization", "Bearer"+TOKEN);
                InputStream odgovor2 = new BufferedInputStream(conn2.getInputStream());
                try {
                    String rezultat2 = convertStreamToString(odgovor2);
                    JSONObject jo = new JSONObject(rezultat2);
                    JSONArray pitanja = jo.getJSONArray("documents");
                    for(int i=0;i<pitanja.length();i++){
                        JSONObject pitanje = pitanja.getJSONObject(i);
                        JSONObject atributi = pitanje.getJSONObject("fields");
                        JSONObject naziv = atributi.getJSONObject("naziv");
                        String nazivPitanja = naziv.getString("stringValue");
                        //System.out.println(nazivPitanja);
                        JSONObject tacanOdg = atributi.getJSONObject("indexTacnog");
                        int indexTacnog = tacanOdg.getInt("integerValue");
                        JSONObject odgovori = atributi.getJSONObject("odgovori");
                        JSONObject odg = odgovori.getJSONObject("arrayValue");
                        JSONArray odgovoriNapitanje = odg.getJSONArray("values");
                        //lista stringova koji predstavljaju odgovore svakog pitanja
                        ArrayList<String> odgovoriPitanjaIzBaze = new ArrayList<>();
                        for(int j=0;j<odgovoriNapitanje.length();j++){
                            JSONObject odgovor = odgovoriNapitanje.getJSONObject(j);
                            String konacanOdg = odgovor.getString("stringValue");
                            odgovoriPitanjaIzBaze.add(konacanOdg);
                        }
                        Pitanje p = new Pitanje(nazivPitanja, nazivPitanja, odgovoriPitanjaIzBaze, odgovoriPitanjaIzBaze.get(indexTacnog));
                        KvizoviAkt.pitanjaIzBaze.add(p);
                    }


                }catch (JSONException e) {
                    e.printStackTrace();
                }

            }catch(IOException e){

            }
            return " ";
        }

        @Override
        protected void onPostExecute(String result) {
            //if (this.dialog.isShowing()) { // if dialog box showing = true
            //  this.dialog.dismiss(); // dismiss it
            //}
            interfejs.krajUcitavanja(result);
        }

        public String convertStreamToString(InputStream is){
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            try{
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodajpitanje_akt);
        pitanje = new Pitanje();

        nazivPitanja = (EditText) findViewById(R.id.etNaziv);
        textOdgovora = (EditText) findViewById(R.id.etOdgovor);
        listaOdg = (ListView) findViewById(R.id.lvOdgovori);
        btnDodaj = (Button) findViewById(R.id.btnDodajOdgovor);
        btnDodajTacan = (Button) findViewById(R.id.btnDodajTacan);
        btnSpasi = (Button) findViewById(R.id.btnDodajPitanje);

        konektovanoNaInternet = konektovanoNaInternet();
        if(konektovanoNaInternet) {
            new UcitajPitanjaIzBaze(new DohvatiIzBaze() {

                @Override
                public void krajUcitavanja(String output) {

                }
            }).execute(" ");
        }else{
            //ucitaj iz sql
            pitanjaSQL = helper.dohvatiPitanjaIzSQL();
        }

        adapterOdgovor = new ArrayAdapter<String>(this, R.layout.odgovor,R.id.textView, listaOdgovora);

        listaOdg.setAdapter(adapterOdgovor);


        nazivPitanja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pitanje.setNaziv(nazivPitanja.getText().toString());
            }
        });

        btnDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dodaje odgovor u listu odgovora i to tekst iz tex2 edittexta
                if(!listaOdgovora.contains(textOdgovora.getText().toString())) {
                    listaOdgovora.add(textOdgovora.getText().toString());
                    adapterOdgovor.notifyDataSetChanged();
                }else{
                    textOdgovora.setBackgroundColor(getResources().getColor(R.color.netacno));

                }


            }
        });

        btnDodajTacan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pitanje.setTacan(textOdgovora.getText().toString());
                if(!listaOdgovora.contains(textOdgovora.getText().toString())){
                    listaOdgovora.add(textOdgovora.getText().toString());

                    adapterOdgovor.notifyDataSetChanged();

                }
                tacno = true;
            }
        });

        btnSpasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ovdje treba verifikacija naziva pitanja i mozda unesenog odgovora
                boolean dobro = verificiraj();
                if(dobro) {

                    pitanje.setNaziv(nazivPitanja.getText().toString());
                    pitanje.setTekstPitanja(nazivPitanja.getText().toString());
                    pitanje.setTacan(textOdgovora.getText().toString());
                    pitanje.setOdgovori(listaOdgovora);

                    dodajPitanje = true;
                    Intent intent = new Intent(DodajPitanjeAkt.this, DodajKvizAkt.class);
                    Bundle naziv = new Bundle();
                    naziv.putSerializable("pitanje", pitanje);
                    intent.putExtras(naziv);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });

        listaOdg.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,int position, long id){
                if(position >= 0 && position < listaOdgovora.size()){
                    listaOdgovora.remove(position);
                    adapterOdgovor.notifyDataSetChanged();
                }
            }
        });

    }

    public boolean verificiraj(){
        boolean dobro = true;
        konektovanoNaInternet = konektovanoNaInternet();
        if(konektovanoNaInternet) {
            if (nazivPitanja.getText().toString().equals("")) {
                nazivPitanja.setBackgroundColor(getResources().getColor(R.color.netacno));
                dobro = false;
            } else {
                if(konektovanoNaInternet) {
                    for (Pitanje p : KvizoviAkt.pitanjaIzBaze) {
                        if (p.getNaziv().equals(nazivPitanja.getText().toString())) {
                            dobro = false;
                            DodajKvizAkt.porukaDijaloga = "Pitanje koje dodajete vec postoji u bazi";
                            openDialog();
                        }
                    }
                }else{
                    for (Pitanje p : KvizoviAkt.pitanjaSQL) {
                        if (p.getNaziv().equals(nazivPitanja.getText().toString())) {
                            dobro = false;
                            DodajKvizAkt.porukaDijaloga = "Pitanje koje dodajete vec postoji u bazi";
                            openDialog();
                        }
                    }
                }
            }
            if (pitanje.getTacan() == null) {
                btnDodajTacan.setBackgroundColor(getResources().getColor(R.color.netacno));
                dobro = false;
            }
        }else{
            dobro = false;
            AlertDialog.Builder builder = new AlertDialog.Builder(DodajPitanjeAkt.this);
            builder.setTitle("Upozorenje!");
            builder.setMessage("Nemate pristup internetu!");
            builder.setNegativeButton("OK", null);
            AlertDialog dijalog = builder.create();
            dijalog.show();
        }

        return dobro;
    }

    public void openDialog(){
        AlertDijalog exampleDialog = new AlertDijalog();
        exampleDialog.show(getSupportFragmentManager(), "example");
    }

    public boolean konektovanoNaInternet() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else
            connected = false;

        return connected;
    }

}
