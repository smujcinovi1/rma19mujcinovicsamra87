package ba.unsa.etf.rma.klase;


import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import ba.unsa.etf.rma.R;

import static ba.unsa.etf.rma.klase.KategorijaDBOpenHelper.KATEGORIJA_NAZIV;

public class AdapterZaSQL extends ResourceCursorAdapter {

    public AdapterZaSQL(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
    }

    //za kategorije

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView kategorije = (TextView) view.findViewById(R.id.etNaziv);
        kategorije.setText(cursor.getColumnIndex(KATEGORIJA_NAZIV));

    }
}

