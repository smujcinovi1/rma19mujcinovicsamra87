package ba.unsa.etf.rma.klase;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.DodajKategorijuAkt;
import ba.unsa.etf.rma.aktivnosti.DodajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.DodajPitanjeAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorijeIzBaze;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kvizoviBaza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.pitanjaIzBaze;

public class MyAsyncTask extends AsyncTask<Object, Integer, Void> {
    //ova klasa se koristi za dodavanje i editovanje kviza u bazi
    Context kontekst;
    
    public MyAsyncTask(Context aktivnost){
        kontekst = aktivnost;
    }



    @Override
    protected Void doInBackground(Object... objects) {
        Kviz kviz = (Kviz) objects[0];
        InputStream is = kontekst.getResources().openRawResource(R.raw.secret);
        try {
            GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));

            credentials.refreshToken();

            String TOKEN = credentials.getAccessToken();
            Log.d("TOKEN ", TOKEN);

            System.out.println(KvizoviAkt.dodaj);
            if(!KvizoviAkt.dodaj){
                //za KVIZ
                System.out.println(KvizoviAkt.dodaj);

                //if(!DodajPitanjeAkt.dodajPitanjeIzMogucih) {
                    String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/Kvizovi/" + kviz.getIdKviza() + "?access_token=";
                    URL urlObject = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                    HttpURLConnection conn = (HttpURLConnection) urlObject.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("PATCH");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");

                    String pitanja = "";
                    pitanja = "\"pitanja\":{\"arrayValue\":{\"values\":[";

                    for (int k = 0; k < kviz.getPitanja().size() - 1; k++) {

                        if (k < kviz.getPitanja().size() - 2) {
                            pitanja += "{ \"stringValue\":\"" + kviz.getPitanja().get(k).getNaziv() + "\"},";
                        } else {
                            pitanja += "{ \"stringValue\":\"" + kviz.getPitanja().get(k).getNaziv() + "\"}";
                            break;
                        }

                    }
                    pitanja += "]";

                    String dokument = "{ \"fields\": {\"naziv\":{\"stringValue\":\"" + kviz.getNaziv() + "\"},\"idKategorije\": {\"stringValue\":\"" + kviz.getKategorija().getNaziv() + "\"}, " + pitanja + "}}}}";

                    try (OutputStream os = conn.getOutputStream()) {
                        byte[] input = dokument.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }

                    int code = conn.getResponseCode();
                    InputStream odgovor = conn.getInputStream();
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                        Log.d("ODGOVOR", response.toString());
                    }
                //}

                //ako izabere neku od ponudjenih kategorija iz spinera ne treba dodati tu kategoriju ponovo u kategorije vec samo azurirati
                if(DodajKategorijuAkt.dodajKAtegoriju) {
                    //za KATEGORIJU

                        String url1 = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/Kategorije?documentId=" + kviz.getKategorija().getNaziv() + "&access_token=";
                        URL urlObject1 = new URL(url1 + URLEncoder.encode(TOKEN, "UTF-8"));
                        HttpURLConnection conn1 = (HttpURLConnection) urlObject1.openConnection();
                        conn1.setDoOutput(true);
                        conn1.setRequestMethod("POST");
                        conn1.setRequestProperty("Content-Type", "application/json");
                        conn1.setRequestProperty("Accept", "application/json");

                        int idIkone = Integer.valueOf(kviz.getKategorija().getId());
                        String dokument1 = "{ \"fields\": {\"naziv\":{\"stringValue\":\"" + kviz.getKategorija().getNaziv() + "\"},\"idIkonice\": {\"integerValue\":\"" + idIkone + "\"}}}";

                        try (OutputStream os = conn1.getOutputStream()) {
                            byte[] input1 = dokument1.getBytes("utf-8");
                            os.write(input1, 0, input1.length);
                        }

                        int code1 = conn1.getResponseCode();
                        InputStream odgovor1 = conn1.getInputStream();
                        try (BufferedReader br1 = new BufferedReader(new InputStreamReader(odgovor1, "utf-8"))) {
                            StringBuilder response1 = new StringBuilder();
                            String responseLine1 = null;
                            while ((responseLine1 = br1.readLine()) != null) {
                                response1.append(responseLine1.trim());
                            }
                            Log.d("ODGOVOR", response1.toString());
                        }
                    }

                if(DodajPitanjeAkt.dodajPitanje) {
                    //za PITANJA
                    int i = 0;
                    for (Pitanje pitanje : kviz.getPitanja()) {
                        if (i < kviz.getPitanja().size() - 1) {
                            String url2 = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/Pitanja?documentId=" + pitanje.getNaziv() + "&access_token=";
                            URL urlObject2 = new URL(url2 + URLEncoder.encode(TOKEN, "UTF-8"));
                            HttpURLConnection conn2 = (HttpURLConnection) urlObject2.openConnection();
                            conn2.setDoOutput(true);
                            conn2.setRequestMethod("POST");
                            conn2.setRequestProperty("Content-Type", "application/json");
                            conn2.setRequestProperty("Accept", "application/json");

                            String odgovori = "";
                            odgovori = "\"odgovori\":{\"arrayValue\":{\"values\":[";
                            int j = 0;
                            for (String odg : pitanje.getOdgovori()) {

                                if (j < pitanje.getOdgovori().size() - 1) {
                                    odgovori += "{ \"stringValue\":\"" + odg + "\"},";
                                } else {

                                    odgovori += "{ \"stringValue\":\"" + odg + "\"}]";
                                    break;
                                }
                                ++j;
                            }
                            int indexTacnog = pitanje.getOdgovori().indexOf(pitanje.getTacan());
                            String dokument2 = "{ \"fields\": {\"naziv\":{\"stringValue\":\"" + pitanje.getNaziv() + "\"},\"indexTacnog\": {\"integerValue\":\"" + indexTacnog + "\"}, " + odgovori + "}}}}";

                            try (OutputStream os = conn2.getOutputStream()) {
                                byte[] input2 = dokument2.getBytes("utf-8");
                                os.write(input2, 0, input2.length);
                            }

                            int code2 = conn2.getResponseCode();
                            InputStream odgovor2 = conn2.getInputStream();
                            try (BufferedReader br2 = new BufferedReader(new InputStreamReader(odgovor2, "utf-8"))) {
                                StringBuilder response2 = new StringBuilder();
                                String responseLine2 = null;
                                while ((responseLine2 = br2.readLine()) != null) {
                                    response2.append(responseLine2.trim());
                                }
                                Log.d("ODGOVOR", response2.toString());
                            }
                        }
                        i++;
                    }
                }
                if(DodajPitanjeAkt.dodajPitanjeIzMogucih){

                    String url3 = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/Kvizovi/"+kviz.getIdKviza()+"?access_token=";
                    URL urlObject3 = new URL(url3 + URLEncoder.encode(TOKEN, "UTF-8"));
                    HttpURLConnection conn3 = (HttpURLConnection) urlObject3.openConnection();
                    conn3.setDoOutput(true);
                    conn3.setRequestMethod("PATCH");
                    conn3.setRequestProperty("Content-Type", "application/json");
                    conn3.setRequestProperty("Accept", "application/json");

                    String pitanja3 = "";
                    pitanja3 = "\"pitanja\":{\"arrayValue\":{\"values\":[";

                    for (int k = 0; k<kviz.getPitanja().size()-1;k++) {

                        if (k < kviz.getPitanja().size() - 2) {
                            pitanja3 += "{ \"stringValue\":\"" + kviz.getPitanja().get(k).getNaziv() + "\"},";
                        } else {
                            pitanja3 += "{ \"stringValue\":\"" + kviz.getPitanja().get(k).getNaziv() + "\"}";
                            break;
                        }

                    }
                    pitanja3 += "]";

                    String dokument3 = "{ \"fields\": {\"naziv\":{\"stringValue\":\"" + kviz.getNaziv() + "\"},\"idKategorije\": {\"stringValue\":\"" + kviz.getKategorija().getNaziv() + "\"}, " + pitanja3 + "}}}}";

                    try(OutputStream os = conn3.getOutputStream()){
                        byte[] input3 = dokument3.getBytes("utf-8");
                        os.write(input3, 0, input3.length);
                    }

                    int code3 = conn3.getResponseCode();
                    InputStream odgovor3 = conn3.getInputStream();
                    try(BufferedReader br3 = new BufferedReader(new InputStreamReader(odgovor3, "utf-8"))){
                        StringBuilder response3 = new StringBuilder();
                        String responseLine3 = null;
                        while((responseLine3 = br3.readLine()) != null){
                            response3.append(responseLine3.trim());
                        }
                        Log.d("ODGOVOR", response3.toString());
                    }
                }
            }else{

                //za KATEGORIJU
                System.out.println("spiner"+DodajKvizAkt.izabraoIzSpinnera);
                System.out.println("dodaj kat"+DodajKategorijuAkt.dodajKAtegoriju);


                if(!DodajKvizAkt.izabraoIzSpinnera && DodajKategorijuAkt.dodajKAtegoriju) {
                    String url1 = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/Kategorije?documentId=" + kviz.getKategorija().getNaziv() + "&access_token=";
                    URL urlObject1 = new URL(url1 + URLEncoder.encode(TOKEN, "UTF-8"));
                    HttpURLConnection conn1 = (HttpURLConnection) urlObject1.openConnection();
                    conn1.setDoOutput(true);
                    conn1.setRequestMethod("POST");
                    conn1.setRequestProperty("Content-Type", "application/json");
                    conn1.setRequestProperty("Accept", "application/json");

                    int idIkone = Integer.valueOf(kviz.getKategorija().getId());

                    String dokument1 = "{ \"fields\": {\"naziv\":{\"stringValue\":\"" + kviz.getKategorija().getNaziv() + "\"},\"idIkonice\": {\"integerValue\":\"" + idIkone + "\"}}}";

                    try (OutputStream os = conn1.getOutputStream()) {
                        byte[] input1 = dokument1.getBytes("utf-8");
                        os.write(input1, 0, input1.length);
                    }

                    int code1 = conn1.getResponseCode();
                    InputStream odgovor1 = conn1.getInputStream();
                    try (BufferedReader br1 = new BufferedReader(new InputStreamReader(odgovor1, "utf-8"))) {
                        StringBuilder response1 = new StringBuilder();
                        String responseLine1 = null;
                        while ((responseLine1 = br1.readLine()) != null) {
                            response1.append(responseLine1.trim());
                        }
                        Log.d("ODGOVOR", response1.toString());
                    }
                }

                //za PITANJA
                if(!DodajPitanjeAkt.dodajPitanjeIzMogucih) {
                    int i = 0;
                    for (Pitanje pitanje : kviz.getPitanja()) {
                        if (i < kviz.getPitanja().size() - 1) {
                            String url2 = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/Pitanja?documentId=" + pitanje.getNaziv() + "&access_token=";
                            URL urlObject2 = new URL(url2 + URLEncoder.encode(TOKEN, "UTF-8"));
                            HttpURLConnection conn2 = (HttpURLConnection) urlObject2.openConnection();
                            conn2.setDoOutput(true);
                            conn2.setRequestMethod("POST");
                            conn2.setRequestProperty("Content-Type", "application/json");
                            conn2.setRequestProperty("Accept", "application/json");

                            String odgovori = "";
                            odgovori = "\"odgovori\":{\"arrayValue\":{\"values\":[";
                            int j = 0;
                            for (String odg : pitanje.getOdgovori()) {

                                if (j < pitanje.getOdgovori().size() - 1) {
                                    odgovori += "{ \"stringValue\":\"" + odg + "\"},";
                                } else {

                                    odgovori += "{ \"stringValue\":\"" + odg + "\"}]";
                                    break;
                                }
                                ++j;
                            }
                            int indexTacnog = pitanje.getOdgovori().indexOf(pitanje.getTacan());
                            String dokument2 = "{ \"fields\": {\"naziv\":{\"stringValue\":\"" + pitanje.getNaziv() + "\"},\"indexTacnog\": {\"integerValue\":\"" + indexTacnog + "\"}, " + odgovori + "}}}}";

                            try (OutputStream os = conn2.getOutputStream()) {
                                byte[] input2 = dokument2.getBytes("utf-8");
                                os.write(input2, 0, input2.length);
                            }

                            int code2 = conn2.getResponseCode();
                            InputStream odgovor2 = conn2.getInputStream();
                            try (BufferedReader br2 = new BufferedReader(new InputStreamReader(odgovor2, "utf-8"))) {
                                StringBuilder response2 = new StringBuilder();
                                String responseLine2 = null;
                                while ((responseLine2 = br2.readLine()) != null) {
                                    response2.append(responseLine2.trim());
                                }
                                Log.d("ODGOVOR", response2.toString());
                            }
                        }
                        i++;
                    }
                }

                //za KVIZ
                String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/Kvizovi?documentId=" + kviz.getIdKviza() + "&access_token=";
                URL urlObject = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObject.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");


                String pitanja = "";
                pitanja = "\"pitanja\":{\"arrayValue\":{\"values\":[";

                for (int k = 0; k < kviz.getPitanja().size() - 1; k++) {

                    if (k < kviz.getPitanja().size() - 2) {
                        pitanja += "{ \"stringValue\":\"" + kviz.getPitanja().get(k).getNaziv() + "\"},";
                    } else {
                        pitanja += "{ \"stringValue\":\"" + kviz.getPitanja().get(k).getNaziv() + "\"}";
                        break;
                    }

                }
                pitanja += "]";

                String dokument = "{ \"fields\": {\"naziv\":{\"stringValue\":\"" + kviz.getNaziv() + "\"},\"idKategorije\": {\"stringValue\":\"" + kviz.getKategorija().getNaziv() + "\"}, " + pitanja + "}}}}";

                try (OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try (BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    Log.d("ODGOVOR", response.toString());
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String convertStreamToString(InputStream is){
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder response = new StringBuilder();
        String responseLine = null;
        try{
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.toString();
    }



}
