package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.DodajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.DodajPitanjeAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

public class AdapterLista1_dodajKvizAkt extends BaseAdapter {
    private Activity aktivnost;
    private ArrayList lista;
    private static LayoutInflater inflater=null;
    public Resources res;
    Pitanje pitanje=null;
    int i=0;

    public AdapterLista1_dodajKvizAkt(Activity aktivnost, ArrayList lista, Resources res) {
        this.aktivnost = aktivnost;
        this.lista = lista;
        this.res = res;

        inflater = (LayoutInflater) aktivnost.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount(){
        if(lista.size()<=0)
            return 1;
        return lista.size();
    }

    @Override
    public Object getItem(int position){
        return position;
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    public static class ViewHolder{
        //ono sto ce se prikazivati u listi
        public TextView naziv;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;


        //da li je convertview jednak tacnom pitanju, ako jeste postaviti backgroundcolor na zelenu boju
        ViewHolder holder;

        if(convertView==null){
            vi=inflater.inflate(R.layout.dodanopitanje,null);
            holder=new ViewHolder();
            holder.naziv = (TextView) vi.findViewById(R.id.pitanje);

            vi.setTag(holder);

        }else {
            holder = (ViewHolder) vi.getTag();
        }
        if(lista.size()>0){
            pitanje=null;
            pitanje = (Pitanje) lista.get(position);

            holder.naziv.setText(pitanje.getNaziv());
        }



        return  vi;
    }
}

