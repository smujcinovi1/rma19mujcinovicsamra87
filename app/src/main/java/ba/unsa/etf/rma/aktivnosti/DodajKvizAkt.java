package ba.unsa.etf.rma.aktivnosti;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.logging.ConsoleHandler;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.AdapterLista1_dodajKvizAkt;
import ba.unsa.etf.rma.klase.AdapterLista2_dodajKvizAkt;
import ba.unsa.etf.rma.klase.AdapterSpiner_dodajKvizAkt;
import ba.unsa.etf.rma.klase.AlertDijalog;
import ba.unsa.etf.rma.klase.DohvatiIzBaze;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.MyAsyncTask;
import ba.unsa.etf.rma.klase.Pitanje;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorijeIzBaze;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.konektovanoNaInternet;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kvizoviBaza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.pitanjaIzBaze;

public class DodajKvizAkt extends AppCompatActivity {
    public static ArrayList<Pitanje> listaPitanja = new ArrayList<>();//listview
    ArrayList<Pitanje> mogucaPitanja = new ArrayList<>();//listview
    private ArrayList<Kategorija> listakategorija = new ArrayList<>();//spinner
    EditText nazivKviza;
    Button dodajKviz;
    Button importKviz;
    public static Spinner spiner1;
    ListView lista1;
    ListView lista2;
    AdapterSpiner_dodajKvizAkt adapterSpiner;
    AdapterLista1_dodajKvizAkt adapterLista1;
    AdapterLista2_dodajKvizAkt adapterLista2;
    Kviz kviz ;
    public static String imeKategorije;
    Kviz pocetni;
    boolean izaberiKat = false;
    String imeKat = "";
    //Kategorija kat;
    public static String porukaDijaloga = "";
    Kategorija staraKategorija =new Kategorija();
    public static boolean izabraoIzSpinnera = false;

    Resources res;



    public class UcitajPitanjaIzBaze extends AsyncTask<String, Void, String> {

        public UcitajPitanjaIzBaze(DohvatiIzBaze interfejs) {
            //this.aktivnost = aktivnost;
            this.interfejs = interfejs;
        }

        DohvatiIzBaze interfejs = null;
        //private Activity aktivnost;



        @Override
        protected String doInBackground(String... params) {
            InputStream is = getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));

                credentials.refreshToken();

                String TOKEN = credentials.getAccessToken();
                System.out.println(TOKEN);

                if(!pitanjaIzBaze.isEmpty()) {
                    pitanjaIzBaze.clear();
                }

                String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/";
                //Za ucitavanje PITANJA iz baze
                String urlPitanja = url + "Pitanja?access_token=";
                URL urlObject2 = new URL(urlPitanja + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn2 = (HttpURLConnection) urlObject2.openConnection();
                conn2.setRequestProperty("Authorization", "Bearer"+TOKEN);
                InputStream odgovor2 = new BufferedInputStream(conn2.getInputStream());
                try {
                    String rezultat2 = convertStreamToString(odgovor2);
                    JSONObject jo = new JSONObject(rezultat2);
                    JSONArray pitanja = jo.getJSONArray("documents");
                    for(int i=0;i<pitanja.length();i++){
                        JSONObject pitanje = pitanja.getJSONObject(i);
                        JSONObject atributi = pitanje.getJSONObject("fields");
                        JSONObject naziv = atributi.getJSONObject("naziv");
                        String nazivPitanja = naziv.getString("stringValue");
                        //System.out.println(nazivPitanja);
                        JSONObject tacanOdg = atributi.getJSONObject("indexTacnog");
                        int indexTacnog = tacanOdg.getInt("integerValue");
                        JSONObject odgovori = atributi.getJSONObject("odgovori");
                        JSONObject odg = odgovori.getJSONObject("arrayValue");
                        JSONArray odgovoriNapitanje = odg.getJSONArray("values");
                        //lista stringova koji predstavljaju odgovore svakog pitanja
                        ArrayList<String> odgovoriPitanjaIzBaze = new ArrayList<>();
                        for(int j=0;j<odgovoriNapitanje.length();j++){
                            JSONObject odgovor = odgovoriNapitanje.getJSONObject(j);
                            String konacanOdg = odgovor.getString("stringValue");
                            odgovoriPitanjaIzBaze.add(konacanOdg);
                        }
                        Pitanje p = new Pitanje(nazivPitanja, nazivPitanja, odgovoriPitanjaIzBaze, odgovoriPitanjaIzBaze.get(indexTacnog));
                        pitanjaIzBaze.add(p);
                    }


                }catch (JSONException e) {
                    e.printStackTrace();
                }

            }catch(IOException e){

            }
            return " ";
        }

        @Override
        protected void onPostExecute(String result) {
            //if (this.dialog.isShowing()) { // if dialog box showing = true
            //  this.dialog.dismiss(); // dismiss it
            //}
            interfejs.krajUcitavanja(result);
        }

        public String convertStreamToString(InputStream is){
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            try{
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }
    }


    public class UcitajKvizoveIzBaze extends AsyncTask<String, Void, String> {

        public UcitajKvizoveIzBaze(DohvatiIzBaze interfejs) {
            //this.aktivnost = aktivnost;
            this.interfejs = interfejs;
        }

        DohvatiIzBaze interfejs = null;
        //private Activity aktivnost;



        @Override
        protected String doInBackground(String... params) {
            InputStream is = getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));

                credentials.refreshToken();

                String TOKEN = credentials.getAccessToken();
                System.out.println(TOKEN);

                if(!pitanjaIzBaze.isEmpty()) {
                    pitanjaIzBaze.clear();
                }

                String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/";
                //za ucitavanje kvizova iz baze
                String urlKviz = url + "Kvizovi?access_token=";
                URL urlObject = new URL(urlKviz + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObject.openConnection();
                conn.setRequestProperty("Authorization", "Bearer"+TOKEN);
                InputStream odgovor = new BufferedInputStream(conn.getInputStream());

                try {
                    String rezultat = convertStreamToString(odgovor);
                    JSONObject jo = new JSONObject(rezultat);
                    JSONArray kvizovi = jo.getJSONArray("documents");

                    for (int i = 0; i < kvizovi.length(); i++) {
                        JSONObject kvizBaza = kvizovi.getJSONObject(i);
                        JSONObject atributi = kvizBaza.getJSONObject("fields");
                        JSONObject naziv = (JSONObject) atributi.get("naziv");
                        String imeKviza = (String) naziv.get("stringValue");
                        JSONObject id = atributi.getJSONObject("idKategorije");
                        String idKategorije = id.getString("stringValue");//ovo je ustvari naziv kategorije
                        ArrayList<String> pitanja = new ArrayList<>();
                        JSONObject objekat = atributi.getJSONObject("pitanja");
                        JSONObject oObjekat = objekat.getJSONObject("arrayValue");
                        ArrayList<Pitanje> pitanjaKviza = new ArrayList<>();
                        if (!oObjekat.toString().equals("{}")) {
                            JSONArray pitanjaBaza = oObjekat.getJSONArray("values");

                            for (int j = 0; j < pitanjaBaza.length(); j++) {
                                JSONObject pit = pitanjaBaza.getJSONObject(j);
                                String pitanje = pit.getString("stringValue");
                                pitanja.add(pitanje);
                            }
                        }
                        Kviz kv = new Kviz();

                        kv.setIdKviza(String.valueOf((int) 13 + 2*imeKviza.hashCode()));
                        kv.setNaziv(imeKviza);
                        for (Kategorija nova : kategorijeIzBaze) {
                            if (nova.getNaziv().equals(idKategorije)) {
                                kv.setKategorija(nova);
                            }
                        }


                        for (Pitanje p : pitanjaIzBaze) {
                            if (pitanja.contains(p.getNaziv())) {
                                pitanjaKviza.add(p);
                            }
                        }
                        pitanjaKviza.add(new Pitanje("Dodaj Pitanje", null, null, null));
                        kv.setPitanja(pitanjaKviza);
                        kvizoviBaza.add(kv);
                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }

            }catch(IOException e){

            }
            return " ";
        }

        @Override
        protected void onPostExecute(String result) {
            //if (this.dialog.isShowing()) { // if dialog box showing = true
            //  this.dialog.dismiss(); // dismiss it
            //}
            interfejs.krajUcitavanja(result);
        }

        public String convertStreamToString(InputStream is){
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            try{
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodajkviz_akt);
        //kat = new Kategorija();
        kviz = new Kviz();
        res=getResources();
        try {
            Bundle bundle = new Bundle();
            bundle = getIntent().getExtras();
            pocetni = (Kviz) bundle.getSerializable("kviz");
            staraKategorija = pocetni.getKategorija();
            listaPitanja.clear();
            listaPitanja.addAll(pocetni.getPitanja());
        }catch(Exception e){
            listaPitanja.clear();
        }
        konektovanoNaInternet = konektovanoNaInternet();

        if(konektovanoNaInternet) {
            new UcitajPitanjaIzBaze(new DohvatiIzBaze() {

                @Override
                public void krajUcitavanja(String output) {
                    ArrayList<Pitanje> pomocna = new ArrayList<>();
                    pomocna.addAll(pitanjaIzBaze);

                    for (int i = 0; i < listaPitanja.size(); i++) {
                        for (int j = 0; j < pomocna.size(); j++) {
                            if (listaPitanja.get(i).getNaziv().equals(pomocna.get(j).getNaziv())) {
                                pomocna.remove(j);
                            }
                        }
                    }
                    mogucaPitanja.addAll(pomocna);
                    adapterLista2.notifyDataSetChanged();
                    pomocna.clear();
                }
            }).execute(" ");
        }else{
            //iz sql
        }

        dodajKviz = (Button) findViewById(R.id.btnDodajKviz);
        importKviz = (Button) findViewById(R.id.btnImportKviz);
        nazivKviza = (EditText) findViewById(R.id.etNaziv);
        spiner1 = (Spinner) findViewById(R.id.spKategorije);
        lista1 = (ListView) findViewById(R.id.lvDodanaPitanja);
        lista2 = (ListView) findViewById(R.id.lvMogucaPitanja);


        listakategorija.add(new Kategorija("Svi", null));
        listakategorija.addAll(KvizoviAkt.temp);
        listakategorija.add(new Kategorija("Dodaj Kategoriju", null));
        adapterSpiner = new AdapterSpiner_dodajKvizAkt(DodajKvizAkt.this, android.R.layout.simple_spinner_dropdown_item, listakategorija );
        spiner1.setAdapter(adapterSpiner);
        int indexkategorije = 0;
        for(int i=0;i<listakategorija.size();i++){
            if(listakategorija.get(i).getNaziv().equals(staraKategorija.getNaziv())){
                indexkategorije = i;
                break;
            }
        }
        System.out.println(indexkategorije);
        if(!KvizoviAkt.dodaj) {
            spiner1.setSelection(indexkategorije);
            adapterSpiner.notifyDataSetChanged();
        }

        spiner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                imeKategorije = listakategorija.get(position).getNaziv();
                spiner1.setSelection(position);
                if(DodajKvizAkt.imeKategorije.equals("Dodaj Kategoriju")) {
                    izabraoIzSpinnera = false;
                    Intent intent = new Intent(DodajKvizAkt.this, DodajKategorijuAkt.class);
                    DodajKvizAkt.this.startActivityForResult(intent, 4);
                }
                else if(!DodajKvizAkt.imeKategorije.equals("Dodaj Kategoriju")){
                    izabraoIzSpinnera = true;
                 }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        if(listaPitanja.isEmpty()) listaPitanja.add(new Pitanje("Dodaj Pitanje", null, null, null));
        adapterLista1 = new AdapterLista1_dodajKvizAkt(this, listaPitanja, res);


        lista1.setAdapter((ListAdapter) adapterLista1);

        adapterLista2 = new AdapterLista2_dodajKvizAkt(this, mogucaPitanja, res);
        lista2.setAdapter((ListAdapter) adapterLista2);

        lista1.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,int position, long id){
                if(position >= 0 && position < listaPitanja.size()-1){
                    mogucaPitanja.add(listaPitanja.get(position));
                    adapterLista2.notifyDataSetChanged();
                    listaPitanja.remove(position);
                    adapterLista1.notifyDataSetChanged();

                }else if(position == listaPitanja.size()-1){
                    konektovanoNaInternet = konektovanoNaInternet();
                    if(konektovanoNaInternet) {
                        dodajPitanje(position);
                    }else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(DodajKvizAkt.this);
                        builder.setTitle("Upozorenje!");
                        builder.setMessage("Nemate pristup internetu!");
                        builder.setNegativeButton("OK", null);
                        AlertDialog dijalog = builder.create();
                        dijalog.show();
                    }
                }
            }
        });

        lista2.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,int position, long id){
              //  if(mogucaPitanja.size()!=0){
                    if(position == mogucaPitanja.size()-1){
                        listaPitanja.add(listaPitanja.size() - 1, mogucaPitanja.get(position));
                        adapterLista1.notifyDataSetChanged();
                        //mogucaPitanja.clear();
                        mogucaPitanja = new ArrayList<>();
                        adapterLista2.notifyDataSetChanged();
                    }else {
                        listaPitanja.add(listaPitanja.size() - 1, mogucaPitanja.get(position));
                        adapterLista1.notifyDataSetChanged();
                        DodajPitanjeAkt.dodajPitanjeIzMogucih = true;
                        mogucaPitanja.remove(position);
                        adapterLista2.notifyDataSetChanged();
                    }
               // }
            }
        });



        dodajKviz.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                boolean dobro = verify();
                if(dobro){
                    kviz.setPitanja(listaPitanja);
                    //kada dodajem kviz, trebam mu u moguca pitanja upisati sva pitanja koja postoje u bazi
                    //sva pitanja iz baze mogu dobiti tako da od svakog kviza uzmem sva pitanja
                    //tu jos uvijek nece biti dodan ovaj kviz
                    //tako da mu se nece dodati i njegova pitanja
                    //ne bi se moralo ovako ako uspijem nekako dobiti listu pitanja iz one metode za ucitavanje
                    if(mogucaPitanja.isEmpty()) {
                        for (Kviz sviIzBaze : KvizoviAkt.kvizoviTemp) {
                            mogucaPitanja.addAll(sviIzBaze.getPitanja());
                        }
                    }
                    if(!imeKategorije.equals("Dodaj Kategoriju") && izaberiKat==false) {
                        String idKat = "";
                        if (listakategorija.size() >= 2) {
                            for (Kategorija kat : listakategorija) {
                                if (kat.getNaziv().equals(imeKategorije)) {
                                    idKat = kat.getId();
                                    break;
                                }
                            }
                        }
                        kviz.setKategorija(new Kategorija(imeKategorije, idKat));
                        //spiner1.setSelection(listakategorija.indexOf(new Kategorija(imeKategorije, idKat)));

                        //KvizoviAkt.kategorije.add(KvizoviAkt.kategorije.size()-1, new Kategorija(imeKategorije, idKat));
                    }else if(izaberiKat == true){
                        String idKat = "";
                        if (listakategorija.size() >= 2) {
                            for (Kategorija kat : listakategorija) {
                                if (kat.getNaziv().equals(imeKat)) {
                                    idKat = kat.getId();
                                    break;
                                }
                            }
                        }
                        kviz.setKategorija(new Kategorija(imeKat, idKat));
                        //spiner1.setSelection(listakategorija.indexOf(new Kategorija(imeKat, idKat)));
                    }
                    else{
                        int kate = spiner1.getSelectedItemPosition();
                        String idkate = listakategorija.get(kate).getId();
                        Kategorija kat = new Kategorija(listakategorija.get(kate).getNaziv(), idkate);
                        kviz.setKategorija(kat);
                        //spiner1.setSelection(listakategorija.indexOf(kat));

                    }


                    if(KvizoviAkt.dodaj) {
                        kviz.setIdKviza(String.valueOf((int) 13 + 2 * kviz.getNaziv().hashCode()));
                    }else{
                        kviz.setIdKviza(pocetni.getIdKviza());
                    }

                    //sada se treba vratiti na pocetnu aktivnost
                    Intent intent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
                    Bundle naziv = new Bundle();
                    naziv.putSerializable("kviz", kviz);
                    intent.putExtras(naziv);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });

        importKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                konektovanoNaInternet = konektovanoNaInternet();
                if(konektovanoNaInternet) {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("text/*");
                    startActivityForResult(intent, 42);
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(DodajKvizAkt.this);
                    builder.setTitle("Upozorenje!");
                    builder.setMessage("Nemate pristup internetu!");
                    builder.setNegativeButton("OK", null);
                    AlertDialog dijalog = builder.create();
                    dijalog.show();
                }


            }
        });

    }


    // @Override
    //public boolean onCreateOptionsMenu(Menu menu){
    //   getMenuInflater().inflate(R.menu.second, menu);
    //}
    //treba implementirati sta se radi  kada se dodaje nova kategorija, tj nova aktivnost dodajKategorijuAkt

    //otvara aktivnost dodajPitanjeAkt i dodaje pitanje u listaPitanja
    private  void dodajPitanje(int position){
        Intent intent = new Intent(DodajKvizAkt.this, DodajPitanjeAkt.class);
        startActivityForResult(intent, 3);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == 3){
            Pitanje pit = new Pitanje();
            pit = (Pitanje) data.getExtras().getSerializable("pitanje");
            listaPitanja.add(listaPitanja.size() - 1, pit);
            adapterLista1.notifyDataSetChanged();
            kviz.setPitanja(listaPitanja);


            //DodajPitanjeAkt.dodajPitanje=true;
        }
        else if(requestCode == 4){
            Kategorija kat = new Kategorija();
            kat = (Kategorija) data.getExtras().getSerializable("kategorija");
            if(data.getIntExtra("ime", -1)==5){
                //spiner1.setSelection(0);
                adapterSpiner.notifyDataSetChanged();
            }
            else {
                listakategorija.add(listakategorija.size() - 1, kat);
                //KvizoviAkt.temp.add(kat);
                adapterSpiner.notifyDataSetChanged();
                KvizoviAkt.temp.add(kat);
                //DodajKategorijuAkt.dodajKAtegoriju=true;
            }
        }else if(requestCode == 42 && resultCode == Activity.RESULT_OK){
            Uri uri = null;
            if(data != null){
                uri = data.getData();
                //fja za uzimanje podataka iz datoteke
                try {
                    String sadrzaj = readFromUri(uri);
                    System.out.print(sadrzaj);
                    //Kviz noviKviz =  new Kviz();
                    ArrayList<Pitanje> listaPitanjaKviza = new ArrayList<>();
                    String nKviz="";
                    String nazivKategorije= "";
                    String nazivPitanja = "";

                    int brojOdgovora;
                    int indexTacnog;
                    int brojPitanja;
                    boolean alert = false;
                    String[] arr = sadrzaj.split("\\r?\\n");
                    ArrayList<String> redovi = new ArrayList<String>(Arrays.asList(arr));
                    //sada imam u redovima svaki red zasebno odvojen kao poseban element liste
                    if(redovi.size() == 0){
                        porukaDijaloga = "Prazna datoteka!";
                        openDialog();
                        alert = true;
                    }
                    //mogu koristiti isti array
                    arr = redovi.get(0).split(",");
                    ArrayList<String> prviRed = new ArrayList<String>(Arrays.asList(arr));
                    if (prviRed.size() != 3) {
                        porukaDijaloga = "U prvom redu fali neki podatak";
                        openDialog();
                        alert = true;
                    }
                    //sada u prvom redu trebam imati naziv kviza, kategorije i broj pitanja
                    //provjeravamo da li postoji kviz sa tim nazivom
                    nKviz = prviRed.get(0);
                    nazivKviza.setText(nKviz);
                    boolean nepostoji = true;
                    for(Kviz k: kvizoviBaza){
                        if(k.getNaziv().equals(nazivKviza.getText().toString())){
                            nepostoji = false;
                        }
                    }
                    if (!nepostoji) {
                        porukaDijaloga = "Kviz kojeg importujete vec postoji!";
                        openDialog();
                        alert = true;
                    }

                    //vidjeti za kategoriju
                    nazivKategorije = prviRed.get(1);

                    brojPitanja = Integer.parseInt(prviRed.get(2));
                    if (redovi.size() - 1 != brojPitanja) {
                        porukaDijaloga = "Kviz kojeg importujete ima neispravan broj pitanja!";
                        openDialog();
                        alert = true;
                    }
                    if(!alert) {
                        for (String s : redovi) {
                            if (!s.equals(redovi.get(0))) {
                                arr = s.split(",");
                                ArrayList<String> sljedeciRed = new ArrayList<String>(Arrays.asList(arr));
                                nazivPitanja = sljedeciRed.get(0);
                                for(Pitanje p: pitanjaIzBaze){
                                    if(p.getNaziv().equals(nazivPitanja)){
                                        porukaDijaloga = "Kviz kojeg importujete vec ima to pitanje u bazi!";
                                        DodajPitanjeAkt.dodajPitanje = false;

                                        openDialog();
                                        alert=true;
                                        break;
                                    }
                                }
                                if(alert) break;
                                brojOdgovora = Integer.parseInt(sljedeciRed.get(1));
                                indexTacnog = Integer.parseInt(sljedeciRed.get(2));
                                if (indexTacnog < 0 || indexTacnog > brojOdgovora) {
                                    porukaDijaloga = "Kviz kojeg importujete ima neispravan index tacnog odgovora!";
                                    openDialog();
                                    alert = true;
                                    break;

                                } else {
                                    //sada pocinju odgovori za ovo pitanje
                                    if (sljedeciRed.size() - 3 != brojOdgovora) {
                                        porukaDijaloga = "Kviz kojeg importujete ima neispravan broj odgovora!";
                                        openDialog();
                                        alert = true;
                                        break;
                                    } else {
                                        Pitanje pitanje = new Pitanje();
                                        pitanje.setNaziv(nazivPitanja);
                                        //provjeravamo da li vec postoji to pitanje za taj kviz
                                        if (listaPitanja.size() != 0 && listaPitanja.contains(nazivPitanja)) {
                                            porukaDijaloga = "Kviz kojeg importujete ima vise istih pitanja!";
                                            openDialog();
                                            alert = true;
                                            break;
                                        }
                                        ArrayList<String> naziviOdgovora = new ArrayList<>();
                                        for (int i = 3; i < sljedeciRed.size(); i++) {
                                            if(!naziviOdgovora.contains(sljedeciRed.get(i))) {
                                                naziviOdgovora.add(sljedeciRed.get(i));
                                            }
                                            else{
                                                porukaDijaloga = "Ne smiju biti dva odgovora ista!";
                                                openDialog();
                                                alert = true;
                                                break;
                                            }
                                        }

                                        pitanje.setTacan(naziviOdgovora.get(indexTacnog));
                                        pitanje.setOdgovori(naziviOdgovora);
                                        pitanje.setTekstPitanja(nazivPitanja);
                                        listaPitanja.add(listaPitanja.size() - 1, pitanje);
                                        adapterLista1.notifyDataSetChanged();
                                    }
                                }
                            }

                        }
                    }
                    if(alert) {
                        nazivKviza.setText("");
                        listaPitanja.clear();
                        listaPitanja.add(new Pitanje("Dodaj Pitanje", null, null, null));
                    }else{

                        nazivKviza.setText(nKviz);
                        if(!postojiKategorija(nazivKategorije)) {
                            listakategorija.add(listakategorija.size() - 1, new Kategorija(nazivKategorije, "10"));
                            KvizoviAkt.temp.add(new Kategorija(nazivKategorije, null));
                            DodajKategorijuAkt.dodajKAtegoriju = true;
                            izabraoIzSpinnera = false;
                        }
                        else{
                            int indexkat=0;
                            for(int i=0;i<listakategorija.size();i++){
                                if(listakategorija.get(i).getNaziv().equals(nazivKategorije)){
                                    indexkat = i;
                                    DodajKategorijuAkt.dodajKAtegoriju = false;
                                    izabraoIzSpinnera = true;
                                    break;
                                }
                            }
                            spiner1.setSelection(indexkat);
                            izaberiKat = true;
                            imeKat = nazivKategorije;
                        }
                        spiner1.setSelection(listakategorija.size() - 2);

                    }

                } catch (IOException e) {

                }
            }
        }
    }


    private String readFromUri(Uri uri) throws IOException{
        InputStream input = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        StringBuilder string = new StringBuilder();
        String red;
        while ((red = reader.readLine()) != null){
            string.append(red);
            string.append("\n");
        }


        return  string.toString();
    }



    private boolean verify(){
        //samo ako dodaje je edit text prazan

        boolean dobro = true;
        konektovanoNaInternet = konektovanoNaInternet();
        if(!konektovanoNaInternet){
            dobro = false;
            AlertDialog.Builder builder = new AlertDialog.Builder(DodajKvizAkt.this);
            builder.setTitle("Upozorenje!");
            builder.setMessage("Nemate pristup internetu!");
            builder.setNegativeButton("OK", null);
            AlertDialog dijalog = builder.create();
            dijalog.show();
        }else {
            if (KvizoviAkt.dodaj && nazivKviza.getText().toString().equals("")) {//ako dodaje a nije unio tekst
                nazivKviza.setBackgroundColor(getResources().getColor(R.color.netacno));
                dobro = false;
            } else if (KvizoviAkt.dodaj && !nazivKviza.getText().toString().equals("")) {//ako dodaje a postoji kviz sa istim nazivom
                for (Kviz k : kvizoviBaza) {
                    if (k.getNaziv().equals(nazivKviza.getText().toString())) {
                        dobro = false;
                        porukaDijaloga = "Kviz kojeg dodajete vec postoji u bazi!";
                        openDialog();
                    }
                }
                if (dobro) kviz.setNaziv(nazivKviza.getText().toString());
            } else if (!KvizoviAkt.dodaj) {//ako azurira
                if (!nazivKviza.getText().toString().equals(pocetni.getNaziv()) && !nazivKviza.getText().toString().equals("")) {//ako postavlja novo ime provjeri da li vec postoji tako ime kviza
                    for (Kviz k : kvizoviBaza) {
                        if (k.getNaziv().equals(nazivKviza.getText().toString())) {
                            dobro = false;
                            porukaDijaloga = "Kviz kojeg dodajete vec postoji u bazi!";
                            openDialog();
                        }
                    }
                    if (dobro) kviz.setNaziv(nazivKviza.getText().toString());
                } else if (nazivKviza.getText().toString().equals("")) {
                    kviz.setNaziv(pocetni.getNaziv());
                }
            }
        }
        return dobro;
    }



    public void openDialog(){
        AlertDijalog exampleDialog = new AlertDijalog();
        exampleDialog.show(getSupportFragmentManager(), "example");
    }

    private boolean postojiKategorija(String s){
        boolean postoji=false;
        for(Kategorija k: kategorijeIzBaze){
            if(k.getNaziv().equals(s)){
                postoji=true;
                break;
            }
        }
        return postoji;
    }

    public boolean konektovanoNaInternet() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else
            connected = false;

        return connected;
    }

}
