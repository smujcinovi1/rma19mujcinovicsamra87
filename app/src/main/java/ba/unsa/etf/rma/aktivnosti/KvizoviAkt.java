package ba.unsa.etf.rma.aktivnosti;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.AlertDijalog;
import ba.unsa.etf.rma.klase.CustomAdapterKategorija;
import ba.unsa.etf.rma.klase.CustomAdapterKviz;
import ba.unsa.etf.rma.klase.DetailFrag;
import ba.unsa.etf.rma.klase.DohvatiIzBaze;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.KategorijaDBOpenHelper;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.ListaFrag;
import ba.unsa.etf.rma.klase.MyAsyncTask;
import ba.unsa.etf.rma.klase.Pitanje;

import static ba.unsa.etf.rma.klase.KategorijaDBOpenHelper.KATEGORIJA_ID;
import static ba.unsa.etf.rma.klase.KategorijaDBOpenHelper.KATEGORIJA_NAZIV;

public class KvizoviAkt extends AppCompatActivity{
    private static final int MY_CAL_REQ = 0;
    CustomAdapterKviz adapterLista;
    CustomAdapterKategorija adapterSpinner;
    //lista kategorija koje se nalaze u spineru
    public static ArrayList<Kategorija> temp = new ArrayList<>();
    private ArrayList<Kategorija> kategorije = new ArrayList<>();
    //lista svih postojecih kvizova
    public static ArrayList<Kviz> kvizoviTemp = new ArrayList<>();
    private ArrayList<Kviz> sviKvizovi = new ArrayList<>();

    public KvizoviAkt CustomListView=null;

    //lista kategorija ucitanih iz baze koju mogu koristiti
    public static ArrayList<Kategorija> kategorijeIzBaze = new ArrayList<>();
    //lista pitanja ucitanih iz baze koju mogu koristiti
    public static ArrayList<Pitanje> pitanjaIzBaze = new ArrayList<>();
    //lista kvizova ucitanih iz baze
    public static ArrayList<Kviz> kvizoviBaza = new ArrayList<>();
    //lista u kojoj su kvizovi iz odabrane kategorije
    public static ArrayList<Kviz> kvizoviOdredjeneKategorije = new ArrayList<>();
    public static boolean konektovanoNaInternet = false;

    public static ArrayList<Kviz> kvizoviSQL = new ArrayList<>();
    public static ArrayList<Kategorija> kategorijeSQL = new ArrayList<>();
    public static ArrayList<Pitanje> pitanjaSQL = new ArrayList<>();


    public class UcitajIzBaze extends AsyncTask<String, Void, String> {
        //ova klasa ucitava iz baze u aplikaciju

        DohvatiIzBaze interfejs = null;

        public UcitajIzBaze(DohvatiIzBaze interfejs) {

            this.interfejs = interfejs;

        }

        /* @Override
         protected void onPreExecute() {
             this.dialog.setMessage("Dohvatanje podataka iz baze...");
             this.dialog.show();
         }*/
        @Override
        protected String doInBackground(String... params) {
           // boolean zavrseno = false;
            //ucitava sve sto se nalazi u bazi

            InputStream is = getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));

                credentials.refreshToken();

                String TOKEN = credentials.getAccessToken();
                System.out.println(TOKEN);


                String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/";
                if(!kategorijeIzBaze.isEmpty() || !kvizoviBaza.isEmpty() || !pitanjaIzBaze.isEmpty()){
                    kategorijeIzBaze.clear();
                    kvizoviBaza.clear();
                    pitanjaIzBaze.clear();
                }

                //Za ucitavanje KATEGORIJA iz baze
                String urlKategorije = url + "Kategorije?access_token=";
                URL urlObject1 = new URL(urlKategorije + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn1 = (HttpURLConnection) urlObject1.openConnection();
                conn1.setRequestProperty("Authorization", "Bearer"+TOKEN);
                InputStream odgovor1 = new BufferedInputStream(conn1.getInputStream());

                try {
                    String rezultat1 = convertStreamToString(odgovor1);
                    JSONObject jo = new JSONObject(rezultat1);
                    JSONArray kategorije = jo.getJSONArray("documents");
                    for(int i=0;i<kategorije.length();i++){
                        JSONObject kategorija = kategorije.getJSONObject(i);
                        JSONObject atributi = kategorija.getJSONObject("fields");
                        JSONObject naziv = atributi.getJSONObject("naziv");
                        String nazivKategorije = naziv.getString("stringValue");
                        JSONObject id = atributi.getJSONObject("idIkonice");
                        int idIkone = id.getInt("integerValue");
                        //System.out.println(idIkone);
                        Kategorija nova = new Kategorija(nazivKategorije, String.valueOf(idIkone));
                        kategorijeIzBaze.add(nova);
                    }


                }catch (JSONException e) {
                    e.printStackTrace();
                }
                //Za ucitavanje PITANJA iz baze
                String urlPitanja = url + "Pitanja?access_token=";
                URL urlObject2 = new URL(urlPitanja + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn2 = (HttpURLConnection) urlObject2.openConnection();
                conn2.setRequestProperty("Authorization", "Bearer"+TOKEN);
                InputStream odgovor2 = new BufferedInputStream(conn2.getInputStream());
                try {
                    String rezultat2 = convertStreamToString(odgovor2);
                    JSONObject jo = new JSONObject(rezultat2);
                    JSONArray pitanja = jo.getJSONArray("documents");
                    for(int i=0;i<pitanja.length();i++){
                        JSONObject pitanje = pitanja.getJSONObject(i);
                        JSONObject atributi = pitanje.getJSONObject("fields");
                        JSONObject naziv = atributi.getJSONObject("naziv");
                        String nazivPitanja = naziv.getString("stringValue");
                        //System.out.println(nazivPitanja);
                        JSONObject tacanOdg = atributi.getJSONObject("indexTacnog");
                        int indexTacnog = tacanOdg.getInt("integerValue");
                        JSONObject odgovori = atributi.getJSONObject("odgovori");
                        JSONObject odg = odgovori.getJSONObject("arrayValue");
                        JSONArray odgovoriNapitanje = odg.getJSONArray("values");
                        //lista stringova koji predstavljaju odgovore svakog pitanja
                        ArrayList<String> odgovoriPitanjaIzBaze = new ArrayList<>();
                        for(int j=0;j<odgovoriNapitanje.length();j++){
                            JSONObject odgovor = odgovoriNapitanje.getJSONObject(j);
                            String konacanOdg = odgovor.getString("stringValue");
                            odgovoriPitanjaIzBaze.add(konacanOdg);
                        }
                        Pitanje p = new Pitanje(nazivPitanja, nazivPitanja, odgovoriPitanjaIzBaze, odgovoriPitanjaIzBaze.get(indexTacnog));
                        pitanjaIzBaze.add(p);
                    }


                }catch (JSONException e) {
                    e.printStackTrace();
                }

                //za ucitavanje kvizova iz baze
                String urlKviz = url + "Kvizovi?access_token=";
                URL urlObject = new URL(urlKviz + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObject.openConnection();
                conn.setRequestProperty("Authorization", "Bearer"+TOKEN);
                InputStream odgovor = new BufferedInputStream(conn.getInputStream());

                try {
                    String rezultat = convertStreamToString(odgovor);
                    JSONObject jo = new JSONObject(rezultat);
                    JSONArray kvizovi = jo.getJSONArray("documents");

                    for (int i = 0; i < kvizovi.length(); i++) {
                        JSONObject kvizBaza = kvizovi.getJSONObject(i);
                        JSONObject atributi = kvizBaza.getJSONObject("fields");
                        JSONObject naziv = (JSONObject) atributi.get("naziv");
                        String imeKviza = (String) naziv.get("stringValue");
                        JSONObject id = atributi.getJSONObject("idKategorije");
                        String idKategorije = id.getString("stringValue");//ovo je ustvari naziv kategorije
                        ArrayList<String> pitanja = new ArrayList<>();
                        JSONObject objekat = atributi.getJSONObject("pitanja");
                        JSONObject oObjekat = objekat.getJSONObject("arrayValue");
                        ArrayList<Pitanje> pitanjaKviza = new ArrayList<>();
                        if (!oObjekat.toString().equals("{}")) {
                            JSONArray pitanjaBaza = oObjekat.getJSONArray("values");

                            for (int j = 0; j < pitanjaBaza.length(); j++) {
                                JSONObject pit = pitanjaBaza.getJSONObject(j);
                                String pitanje = pit.getString("stringValue");
                                pitanja.add(pitanje);
                            }
                        }
                        Kviz kv = new Kviz();

                        kv.setIdKviza(String.valueOf((int) 13 + 2*imeKviza.hashCode()));
                        kv.setNaziv(imeKviza);
                        for (Kategorija nova : kategorijeIzBaze) {
                            if (nova.getNaziv().equals(idKategorije)) {
                                kv.setKategorija(nova);
                            }
                        }


                        for (Pitanje p : pitanjaIzBaze) {
                            if (pitanja.contains(p.getNaziv())) {
                                pitanjaKviza.add(p);
                            }
                        }
                        pitanjaKviza.add(new Pitanje("Dodaj Pitanje", null, null, null));
                        kv.setPitanja(pitanjaKviza);
                        kvizoviBaza.add(kv);

                        //ovdje odmah upisi u sql

                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }

                // Log.d("ODGOVOR", response.toString());

               // zavrseno = true;
            }catch(IOException e){

            }

            return "";
        }
        public String convertStreamToString(InputStream is){
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            try{
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }
        @Override
        protected void onPostExecute(String result) {
            //if (this.dialog.isShowing()) { // if dialog box showing = true
            //  this.dialog.dismiss(); // dismiss it
            //}
            interfejs.krajUcitavanja(result);
        }
    }

    public class UcitajKvizoveOdredjeneKategorije extends AsyncTask<String, Void, String> {

        public UcitajKvizoveOdredjeneKategorije(DohvatiIzBaze interfejs) {
            //this.aktivnost = aktivnost;
            this.interfejs = interfejs;
        }

        DohvatiIzBaze interfejs = null;
        //private Activity aktivnost;



        @Override
        protected String doInBackground(String... strings) {
            InputStream is = getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));

                credentials.refreshToken();

                String TOKEN = credentials.getAccessToken();
                System.out.println(TOKEN);

                kvizoviOdredjeneKategorije.clear();

                String query = "{\n" +
                        "    \"structuredQuery\": {\n" +
                        "        \"where\" : {\n" +
                        "            \"fieldFilter\" : { \n" +
                        "                \"field\": {\"fieldPath\": \"idKategorije\"}, \n" +
                        "                \"op\":\"EQUAL\", \n" +
                        "                \"value\": {\"stringValue\": \"" + strings[0] + "\"}\n" +
                        "            }\n" +
                        "        },\n" +
                        "        \"select\": { \"fields\": [ {\"fieldPath\": \"idKategorije\"}, {\"fieldPath\": \"naziv\"}, {\"fieldPath\": \"pitanja\"}] },\n" +
                        "        \"from\": [{\"collectionId\": \"Kvizovi\"}],\n" +
                        "       \"limit\": 1000 \n" +
                        "    }\n" +
                        "}";

                String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents:runQuery?access_token=";
                //Za ucitavanje PITANJA iz baze

                URL urlObject2 = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn2 = (HttpURLConnection) urlObject2.openConnection();
                conn2.setDoInput(true);
                conn2.setRequestMethod("POST");
                conn2.setRequestProperty("Content-Type", "application/json");
                conn2.setRequestProperty("Accept", "application/json");
                //InputStream odgovor2 = new BufferedInputStream(conn2.getInputStream());
                try(OutputStream os = conn2.getOutputStream()) {
                    byte[] input = query.getBytes("utf-8");
                    os.write(input, 0, input.length);

                }

                int code = conn2.getResponseCode();
                InputStream in = conn2.getInputStream();
                String rezultatFiltriraj = convertStreamToString(in);
                rezultatFiltriraj = "{ \"documents\": " + rezultatFiltriraj + "}";
                JSONObject jo = null;
                if(!rezultatFiltriraj.equals("{}")) {
                    jo = new JSONObject(rezultatFiltriraj);
                    JSONArray kvizovi = jo.getJSONArray("documents");

                    for (int i = 0; i < kvizovi.length(); i++) {
                        JSONObject kvizBaza = kvizovi.getJSONObject(i);

                        if (kvizBaza.has("document")) {

                            JSONObject kvizzBaza = kvizBaza.getJSONObject("document");
                            String nazivKviza = kvizzBaza.getString("name");
                            int brojac = 0;
                            for (int j = 0; j < nazivKviza.length(); j++) {
                                if (nazivKviza.charAt(j) == '/') {
                                    brojac++;
                                }
                                if (brojac == 6) {
                                    nazivKviza = nazivKviza.substring(++j, nazivKviza.length());
                                }
                            }

                            JSONObject atributi = kvizzBaza.getJSONObject("fields");
                            JSONObject naziv = (JSONObject) atributi.get("naziv");
                            String imeKviza = (String) naziv.get("stringValue");
                            JSONObject id = atributi.getJSONObject("idKategorije");
                            String idKategorije = id.getString("stringValue");//ovo je ustvari naziv kategorije
                            ArrayList<String> pitanja = new ArrayList<>();
                            JSONObject objekat = atributi.getJSONObject("pitanja");
                            JSONObject oObjekat = objekat.getJSONObject("arrayValue");
                            ArrayList<Pitanje> pitanjaKviza = new ArrayList<>();
                            if (!oObjekat.toString().equals("{}")) {
                                JSONArray pitanjaBaza = oObjekat.getJSONArray("values");

                                for (int j = 0; j < pitanjaBaza.length(); j++) {
                                    JSONObject pit = pitanjaBaza.getJSONObject(j);
                                    String pitanje = pit.getString("stringValue");
                                    pitanja.add(pitanje);
                                }
                            }
                            Kviz kv = new Kviz();

                            kv.setIdKviza(String.valueOf((int) 13 + 2 * imeKviza.hashCode()));
                            kv.setNaziv(imeKviza);
                            for (Kategorija nova : kategorijeIzBaze) {
                                if (nova.getNaziv().equals(idKategorije)) {
                                    kv.setKategorija(nova);
                                }
                            }


                            for (Pitanje p : pitanjaIzBaze) {
                                if (pitanja.contains(p.getNaziv())) {
                                    pitanjaKviza.add(p);
                                }
                            }
                            pitanjaKviza.add(new Pitanje("Dodaj Pitanje", null, null, null));
                            kv.setPitanja(pitanjaKviza);
                            kvizoviOdredjeneKategorije.add(kv);
                            System.out.println("Velicina kvizova iz kategorije" + kvizoviOdredjeneKategorije.size());
                        }

                    }
                }

            }catch(IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return " ";
        }

        @Override
        protected void onPostExecute(String result) {
            //if (this.dialog.isShowing()) { // if dialog box showing = true
            //  this.dialog.dismiss(); // dismiss it
            //}
            interfejs.krajUcitavanja(result);
        }

        public String convertStreamToString(InputStream is){
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            try{
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }
    }


    Resources res;
    Spinner spiner;
    ListView lista;
    public static boolean dodaj;
    int pozicija;
    public static boolean bigScreen;
    FrameLayout listaF, gridF;

    public static KategorijaDBOpenHelper helper;
    SQLiteDatabase SQLBaza;

    public static boolean ucitajIzBaze=false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CustomListView = this;

        res = getResources();

        System.out.println("nestoo");

        helper = new KategorijaDBOpenHelper(this);
        try{
            SQLBaza = helper.getWritableDatabase();
        }catch (SQLException e){
            SQLBaza = helper.getReadableDatabase();
        }

        listaF = (FrameLayout) findViewById(R.id.listPlace);
        if (listaF != null) {
            bigScreen = true;
        } else bigScreen = false;

        System.out.println("nestoo");

        if(bigScreen){
            FragmentManager fm = getSupportFragmentManager();

            ListaFrag lf = new ListaFrag();
            DetailFrag df = new DetailFrag();

            fm.beginTransaction().replace(R.id.listPlace, lf).commit();
            fm.beginTransaction().replace(R.id.detailPlace, df).commit();
        }else {

            System.out.println("nestoo");

            konektovanoNaInternet = konektovanoNaInternet();

            if(konektovanoNaInternet) {
                new UcitajIzBaze(new DohvatiIzBaze() {

                    @Override
                    public void krajUcitavanja(String output) {
                        kvizoviTemp.addAll(kvizoviBaza);
                        System.out.println(kvizoviTemp.size() + "    " + kvizoviBaza.size());
                        temp.addAll(kategorijeIzBaze);
                        System.out.println(temp.size() + "    " + kategorijeIzBaze.size());
                        kategorije.addAll(kategorijeIzBaze);
                        sviKvizovi.addAll(kvizoviBaza);
                        sviKvizovi.add(sviKvizovi.get(0));
                        sviKvizovi.remove(0);
                        System.out.println(sviKvizovi.size() + "    " + kategorije.size());

                        adapterLista.notifyDataSetChanged();
                        adapterSpinner.notifyDataSetChanged();

                        //kada je konektovano na internet i ucitano iz firebase treba to odmah proslijediti u sql
                        helper.obrisiSve();
                        for(int i=0; i< kategorije.size(); i++){
                            if(!kategorije.get(i).getNaziv().equals("Svi")) {
                                helper.upisiKategorijuUSQL(kategorije.get(i));
                                System.out.println("upisuje kat");
                            }
                        }
                        for(int i=0; i< pitanjaIzBaze.size(); i++){
                            helper.upisiPitanjeUSQL(pitanjaIzBaze.get(i));
                            System.out.println("upisuje pit");

                        }

                        for(int i=0; i < kvizoviBaza.size();i++){
                            if(!kvizoviBaza.get(i).getNaziv().equals("Dodaj Kviz")){
                                helper.upisiKvizUSQL(kvizoviBaza.get(i));
                                System.out.println("upisuje kviz");
                            }
                        }

                    }
                }).execute(" ");

            }else{
                /*
                //ako nije konektovano na internet treba se ucitati is sql
                //kvizovi u sviKvizovi
                //kateogrije u spinner
                kvizoviSQL = helper.dohvatiKvizoveIzSQL();
                kategorijeSQL = helper.dohvatiKategorijeIzSQL();
                pitanjaSQL = helper.dohvatiPitanjaIzSQL();

                sviKvizovi.clear();
                kategorije.clear();
                kvizoviTemp.clear();
                temp.clear();
                kvizoviTemp.addAll(kvizoviSQL);
                System.out.println(kvizoviTemp.size() + "   kvizovi sql " + kvizoviSQL.size());
                temp.addAll(kategorijeSQL);
                System.out.println(temp.size() + "    kat sql" + kategorijeSQL.size());
                kategorije.addAll(kategorijeSQL);
                sviKvizovi.addAll(kvizoviSQL);
                //sviKvizovi.add(sviKvizovi.get(0));
                //sviKvizovi.remove(0);
                System.out.println(sviKvizovi.size() + "    " + kategorije.size());

                adapterLista = new CustomAdapterKviz(CustomListView, sviKvizovi, res);
                adapterLista.notifyDataSetChanged();
                adapterSpinner = new CustomAdapterKategorija(KvizoviAkt.this, android.R.layout.simple_spinner_dropdown_item, kategorije);
                adapterSpinner.notifyDataSetChanged();
*/
            }
            System.out.println("nestoo");


            spiner = (Spinner) findViewById(R.id.spPostojeceKategorije);
            lista = (ListView) findViewById(R.id.lvKvizovi);

            System.out.println("nestoo");

            konektovanoNaInternet = konektovanoNaInternet();
            if(!konektovanoNaInternet){
                kvizoviSQL = helper.dohvatiKvizoveIzSQL();
                kategorijeSQL = helper.dohvatiKategorijeIzSQL();
                pitanjaSQL = helper.dohvatiPitanjaIzSQL();

                System.out.println("broj pitanja u kvizovisql kavizoviakt za drug po redu kviz" + kvizoviSQL.get(2).getPitanja().size());

                sviKvizovi.clear();
                kategorije.clear();
                kvizoviTemp.clear();
                temp.clear();
                kvizoviTemp.addAll(kvizoviSQL);
                System.out.println(kvizoviTemp.size() + "   kvizovi sql " + kvizoviSQL.size());
                temp.addAll(kategorijeSQL);
                System.out.println(temp.size() + "    kat sql" + kategorijeSQL.size());
                //kategorije.addAll(kategorijeSQL);
                //sviKvizovi.addAll(kvizoviSQL);
                //sviKvizovi.add(sviKvizovi.get(0));
                //sviKvizovi.remove(0);
                System.out.println(sviKvizovi.size() + "    " + kategorije.size());
            }
            //u listu kategorija treba na prvo mjesto dodati kategoriju svi
            kategorije.add(0, new Kategorija("Svi", null));
            kategorije.addAll(temp);
            System.out.println("nestoo");

            //adapter za spiner u koji se dodaju kategorije(element_liste_kategrija)
            adapterSpinner = new CustomAdapterKategorija(KvizoviAkt.this, android.R.layout.simple_spinner_dropdown_item, kategorije);
            adapterSpinner.notifyDataSetChanged();
            //adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spiner.setAdapter(adapterSpinner);

            sviKvizovi.addAll(kvizoviTemp);
            sviKvizovi.add(new Kviz("Dodaj Kviz", null, null));
            System.out.println(sviKvizovi.size());

            //adapter za listu svih kvizova u koju ce se dodavati kvizovi novi
            adapterLista = new CustomAdapterKviz(CustomListView, sviKvizovi, res);
            adapterLista.notifyDataSetChanged();


            lista.setAdapter((ListAdapter) adapterLista);

            spiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    if (position >= 0 && position < kategorije.size()) {
                        getSelectedCategoryData(kategorije.get(position).getNaziv());
                        System.out.println(kategorije.get(position).getNaziv());
                    } else {
                        Toast.makeText(KvizoviAkt.this, "Selected Category does not exist!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            //dugi klik za edit i dodavanje
            lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                    konektovanoNaInternet = konektovanoNaInternet();
                    if(konektovanoNaInternet) {
                        if (position >= 0 && position < sviKvizovi.size() - 1) {
                            azurirajKviz(position);
                            pozicija = position;
                            
                            dodaj = false;
                        } else if (position == sviKvizovi.size() - 1) {
                            dodajKviz(position);
                            dodaj = true;
                        }
                    }else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(KvizoviAkt.this);
                        builder.setTitle("Upozorenje!");
                        builder.setMessage("Nemate pristup internetu!");
                        builder.setNegativeButton("OK", null);
                        AlertDialog dijalog = builder.create();
                        dijalog.show();
                    }
                    return true;
                }
            });

            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position != sviKvizovi.size() - 1) {


                        TreeMap<String, String> dogadjajPocetak = getDataFromEventTable(view);
                        //trebam proci kroz mapu i vidjeti koji od ucitanih dogadjaja pocinje isti dan kada i igra kviz
                        //i ako je trajanje kviza duze od vremena za koliko pocinje event

                        //prvo odredimo danasnji datum
                        Date danasnjiDatum = new Date();
                        //od ovog datuma nam treba samo datum a ne i vrijeme i sati
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                        for(Map.Entry<String, String> mapa: dogadjajPocetak.entrySet()){
                            long time = Long.parseLong(mapa.getValue());
                            Date datum = new Date(time);

                            //ako je isti datum
                            if(format.format(datum).equals(format.format(danasnjiDatum))){
                                int trajanjeKviza = 0;
                             //  if(sviKvizovi.get(position).getPitanja().size() - 1 > 1) {
                                    trajanjeKviza = (int) Math.ceil((double)(sviKvizovi.get(position).getPitanja().size() - 1) / 2);
                                    System.out.println("trajanje + " + trajanjeKviza);
                             //   }
                              //  else if(sviKvizovi.get(position).getPitanja().size() - 1 == 1){
                              //      trajanjeKviza = 1;
                               // }

                                long trajanjeKvizaUMiliSekundama = TimeUnit.MINUTES.toMillis(trajanjeKviza);
                                long vrijemeDoDogadjaja = datum.getTime();
                                long trenutnoVrijeme = danasnjiDatum.getTime();
                                int vrijemePreostaloOdTrenutnogDoDogadjaja = (int) (TimeUnit.MILLISECONDS.toMinutes(vrijemeDoDogadjaja - trenutnoVrijeme) + 1);
                                if(trenutnoVrijeme < vrijemeDoDogadjaja && (trenutnoVrijeme + trajanjeKvizaUMiliSekundama) > vrijemeDoDogadjaja){
                                    //sada treba izbaciti alert
                                    AlertDialog.Builder builder = new AlertDialog.Builder(KvizoviAkt.this);
                                    builder.setTitle("Upozorenje!");
                                    builder.setMessage("Imate dogadjaj u kalendaru za " + vrijemePreostaloOdTrenutnogDoDogadjaja + " minuta!");
                                    builder.setNegativeButton("OK", null);
                                    AlertDialog dijalog = builder.create();
                                    dijalog.show();
                                    return;
                                }

                            }
                        }
/*
                        konektovanoNaInternet = konektovanoNaInternet();
                        if(konektovanoNaInternet){
                            sviKvizovi.addAll(helper.dohvatiKvizoveIzSQL());
                        }
                        */
                        System.out.println("proslijedjeni kkviz" + sviKvizovi.get(position).getNaziv() + "broj pitanja tog kviza" + sviKvizovi.get(position).getPitanja().size()+ "kategorija tog kviza" + sviKvizovi.get(position).getKategorija().getNaziv());
                        Intent i = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("proslijedi", sviKvizovi.get(position));
                        i.putExtras(bundle);
                        startActivity(i);
                    }
                }
            });
        }

        System.out.println("nesto");
    }



    private void getSelectedCategoryData(final String naziv){
        konektovanoNaInternet = konektovanoNaInternet();
        if(konektovanoNaInternet) {
            new UcitajKvizoveOdredjeneKategorije(new DohvatiIzBaze() {

                @Override
                public void krajUcitavanja(String output) {
                    if (!naziv.equals("Svi")) {
                        sviKvizovi.clear();
                        sviKvizovi.addAll(kvizoviOdredjeneKategorije);
                        sviKvizovi.add(new Kviz("Dodaj Kviz", null, null));
                        adapterLista.notifyDataSetChanged();
                    } else {
                        sviKvizovi.clear();
                        new UcitajIzBaze(new DohvatiIzBaze() {
                            @Override
                            public void krajUcitavanja(String output) {
                                sviKvizovi.addAll(kvizoviBaza);
                                sviKvizovi.add(new Kviz("Dodaj Kviz", null, null));
                                adapterLista.notifyDataSetChanged();
                            }
                        }).execute(" ");

                    }
                }
            }).execute(naziv);
        }else{
            //ucitaj iz sql
            sviKvizovi.clear();
            kvizoviTemp.clear();
            kvizoviSQL = helper.dohvatiKvizoveIzSQL();
            kvizoviTemp.addAll(kvizoviSQL);
            System.out.println("kvizovi sql size: "+kvizoviSQL.size());

            for (Kviz kviz : kvizoviTemp) {
                if (kviz.getKategorija().getNaziv().equals(naziv) || naziv.equals("Svi")) {
                    sviKvizovi.add(kviz);
                }
            }
            sviKvizovi.add(new Kviz("Dodaj Kviz", null, null));
            adapterLista.notifyDataSetChanged();

        }

    }

    private void azurirajKviz(int position){
        //DodajKvizAkt.spiner1.setSelection(temp.indexOf(kvizoviTemp.get(position)));
        Intent i = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
        Bundle bundle= new Bundle();
        bundle.putSerializable("kviz", sviKvizovi.get(position));
        i.putExtras(bundle);
        startActivityForResult(i, 1);
    }

    private void dodajKviz(int position){
        Intent i = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
        startActivityForResult(i, 9);
        //ne treba ovdje nista vise
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        // super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == 1 && resultCode == RESULT_OK){
            Kviz kv = new Kviz();
            kv = (Kviz) data.getExtras().getSerializable("kviz");
            if(!dodaj) {
                kvizoviTemp.set(pozicija, kv);
                new MyAsyncTask(this).execute(kv);
            }

        }
        else if(requestCode == 9 && resultCode == RESULT_OK){
            Kviz kv = new Kviz();
            kv = (Kviz) data.getExtras().getSerializable("kviz");
            if(dodaj) {
                kvizoviTemp.add(kv);

                new MyAsyncTask(this).execute(kv);
            }

        }
        else if(requestCode == 100 && resultCode == RESULT_OK){
            //dodaje

            Kviz kv = new Kviz();
            kv = (Kviz) data.getExtras().getSerializable("kviz");
           // System.out.println(kv.getNaziv());
            Bundle bundle = new Bundle();
            if(dodaj)
            kvizoviTemp.add(kvizoviTemp.size()-1, kv);
            bundle.putSerializable("proslijedii",kv);
            DetailFrag df = new DetailFrag();
            df.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.detailPlace, df).commitAllowingStateLoss();
            ListaFrag.adapter.notifyDataSetChanged();
            DetailFrag.adapter.notifyDataSetChanged();
            boolean postojiK=false;
            for(Kategorija s: temp){
                if(s.getNaziv().equals(kv.getKategorija().getNaziv())){
                    postojiK = true;
                    break;
                }
            }
            if(!postojiK){
                temp.add(kv.getKategorija());
            }


            ListaFrag lf = new ListaFrag();
            Bundle bundle1 = new Bundle();
            bundle1.putSerializable("kat",temp);
            lf.setArguments(bundle1);
            getSupportFragmentManager().beginTransaction().replace(R.id.listPlace, lf);



        }
            if(listaF == null) {
                sviKvizovi.clear();
                sviKvizovi.addAll(kvizoviTemp);
                sviKvizovi.add(new Kviz("Dodaj Kviz", null, null));
                adapterLista.notifyDataSetChanged();
                kategorije.clear();
                kategorije.add(new Kategorija("Svi", null));
                kategorije.addAll(temp);
                //spiner.setSelection(0);
                adapterSpinner.notifyDataSetChanged();
            }

    }


    private void napuni() {
        kategorije.add(new Kategorija("Svi", "0"));
        kategorije.add(new Kategorija("Osnovna kategorija", "1"));
        kategorije.add(new Kategorija("Pomocna kategorija", "2"));
        sviKvizovi.add(new Kviz("kviz1",null, new Kategorija("2")));
        sviKvizovi.add(new Kviz("kviz2",null, new Kategorija( "2")));
        sviKvizovi.add(new Kviz("kviz3",null, new Kategorija("1")));
        sviKvizovi.add(new Kviz("kviz4",null, new Kategorija("2")));

    }

    public TreeMap<String, String> getDataFromEventTable(View v) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALENDAR}, MY_CAL_REQ);
        }


       //mapa u koju cu stavljati svaki event iz dogadjaja i njegov pocetak
        TreeMap<String, String> dogadjajPocetak = new TreeMap<String, String>();
        Cursor cur = null;
        ContentResolver cr = getContentResolver();

        String[] mProjection =
                {
                        CalendarContract.Events.TITLE,
                        //CalendarContract.Events.EVENT_LOCATION,
                        CalendarContract.Events.DTSTART,
                        //CalendarContract.Events.DTEND,
                };

        //Uri uri = CalendarContract.Events.CONTENT_URI;
       // String selection = CalendarContract.Events.EVENT_LOCATION + " = ? ";
        //String[] selectionArgs = new String[]{"London"};

        cur = cr.query(CalendarContract.Events.CONTENT_URI, mProjection, null, null, null);

        while (cur.moveToNext()) {
            String dogadjaj = cur.getString(0);
            String vrijemePocetka = cur.getString(1);

            //sada mogu staviti to u mapu
            dogadjajPocetak.put(dogadjaj, vrijemePocetka);
        }
        return dogadjajPocetak;

    }

    public void openDialog(){
        AlertDijalog exampleDialog = new AlertDijalog();
        exampleDialog.show(getSupportFragmentManager(), "example");
    }

    public boolean konektovanoNaInternet() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else
            connected = false;

        return connected;
    }

    }