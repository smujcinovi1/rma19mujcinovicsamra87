package ba.unsa.etf.rma.klase;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;

public class RangLista extends Fragment {


    ListView rangLista;
    String nazivKviza;

    ArrayList<String> elementiRangListe = new ArrayList<>();
    ArrayAdapter<String> adapterZaList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle){
        //ovdje se dodjeljuje layout fragmentu, tj.sta ce se nalazitiunutar fragmenta
        View v = inflater.inflate(R.layout.fragment_rang_lista, container, false);
        return  v;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        final IgrajKvizAkt aktivnost = (IgrajKvizAkt) getActivity();

        System.out.println("U novom fragmentu je");

        elementiRangListe.addAll(IgrajKvizAkt.elementiListe);

        System.out.println(elementiRangListe.size());
        rangLista = (ListView) getView().findViewById(R.id.rangLista);

        adapterZaList = new ArrayAdapter<String>(getView().getContext(), android.R.layout.simple_list_item_1, elementiRangListe);

        rangLista.setAdapter(adapterZaList);

    }


}
