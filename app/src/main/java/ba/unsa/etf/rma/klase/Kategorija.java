package ba.unsa.etf.rma.klase;

import java.io.Serializable;

public class Kategorija implements Serializable {
    private String naziv;
    private String id;
    private int ikona;
    private int indikator=0;

    public Kategorija() {}

    public Kategorija(String naziv, String id) {
        this.naziv=naziv;
        this.id=id;
    }

    public Kategorija(String id){
        this.id=id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object pit) {
        if(pit != null && pit instanceof String) {
            Kategorija kat = (Kategorija) pit;
            return (kat.getNaziv().equals(this.naziv));
        }
        return false;
    }
}
