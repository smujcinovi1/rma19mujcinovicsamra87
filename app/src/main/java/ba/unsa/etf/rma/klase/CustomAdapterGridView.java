package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconHelper;

import java.util.List;

import ba.unsa.etf.rma.R;

public class CustomAdapterGridView extends ArrayAdapter<Kviz> {

    public CustomAdapterGridView(Context context, List<Kviz> list) {
        super(context, 0, list);
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        final Kviz kv = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.element_gridviewa, parent, false);
        }

        TextView ime = (TextView) convertView.findViewById(R.id.textView3);
        if(kv != null) {
            ime.setText(kv.getNaziv());

            if (kv.getPitanja() != null) {
                if (kv.getPitanja().size() > 1) {
                    TextView brojPitanja = (TextView) convertView.findViewById(R.id.textView4);
                    brojPitanja.setText(String.valueOf(kv.getPitanja().size() - 1));
                }
            }

            if (kv.getKategorija() != null) {
                if (kv.getKategorija().getId() != null) {
                    final ImageView ikonaKat = convertView.findViewById(R.id.ikona);
                    Icon ikona;
                    final int id = Integer.parseInt(kv.getKategorija().getId());

                    final IconHelper iconHelper = IconHelper.getInstance(parent.getContext());
                    iconHelper.addLoadCallback(new IconHelper.LoadCallback() {
                        @Override
                        public void onDataLoaded() {
                            ikonaKat.setImageDrawable(iconHelper.getIcon(id).getDrawable(parent.getContext()));
                        }
                    });
                } else {
                    final ImageView ikonaKat = convertView.findViewById(R.id.ikona);
                    Icon ikona;
                    int id = 10;
                    ikona = IconHelper.getInstance(parent.getContext()).getIcon(id);
                    Drawable drawable = ikona.getDrawable(parent.getContext());
                    ikonaKat.setImageDrawable(drawable);
                }
            }
        }
        return convertView;
    }
}
