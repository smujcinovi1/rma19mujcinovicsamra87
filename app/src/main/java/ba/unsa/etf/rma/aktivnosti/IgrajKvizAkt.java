package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.provider.AlarmClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.stream.Stream;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.DohvatiIzBaze;
import ba.unsa.etf.rma.klase.InformacijeFrag;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.PitanjeFrag;
import ba.unsa.etf.rma.klase.RangLista;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.helper;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.konektovanoNaInternet;

public class IgrajKvizAkt extends AppCompatActivity {
    Kviz proslijedjeni;
    public static ArrayList<Pitanje> randomPitanjaKviza;
    public static int brojPitanja = 0;
    int brojPostavljenihPitanja = 0;

    //timer
    Uri timer;


    public static String nazivKvizaRang = "";
    Map<String, Double> mapaIgracProcenat = new HashMap<>();
    //public static Map<Integer, Map<String, Double>> mapaPozicijaMapa = new HashMap<>();
    public static ArrayList<String> elementiListe = new ArrayList<>();

    String imeIgraca = "";
    Context kontekst = this;

    public class UcitavanjeRangListiIzBaze extends AsyncTask<String, Void, String> {

        public UcitavanjeRangListiIzBaze(DohvatiIzBaze interfejs) {
            //this.aktivnost = aktivnost;
            this.interfejs = interfejs;
        }

        DohvatiIzBaze interfejs = null;
        //private Activity aktivnost;


        @Override
        protected String doInBackground(String... params) {
            InputStream is = getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));

                credentials.refreshToken();

                String TOKEN = credentials.getAccessToken();
                System.out.println(TOKEN);

                //mapaPozicijaMapa.clear();
                mapaIgracProcenat.clear();

                String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/";
                //Za ucitavanje PITANJA iz baze
                String urlranglista = url + "Rangliste?access_token=";
                URL urlObject2 = new URL(urlranglista + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn2 = (HttpURLConnection) urlObject2.openConnection();
                conn2.setRequestProperty("Authorization", "Bearer" + TOKEN);
                InputStream odgovor2 = new BufferedInputStream(conn2.getInputStream());
                String kvizz = "";
                try {
                    String rezultat2 = convertStreamToString(odgovor2);
                    JSONObject jo = new JSONObject(rezultat2);
                    if(jo.has("documents")) {
                    JSONArray dokument = jo.getJSONArray("documents");

                        for (int i = 0; i < dokument.length(); i++) {
                            JSONObject name = dokument.getJSONObject(i);
                            System.out.println("dovde radi");
                            //JSONObject name1 = name.getJSONObject("name");
                            JSONObject fields = name.getJSONObject("fields");
                            JSONObject imeKviza = fields.getJSONObject("nazivKviza");
                            kvizz = imeKviza.getString("stringValue");
                            System.out.println(kvizz);
                            nazivKvizaRang = kvizz;
                            JSONObject lista = fields.getJSONObject("lista");
                            JSONObject mapa = lista.getJSONObject("mapValue");
                            if (kvizz.equals(proslijedjeni.getNaziv())) {
                                System.out.println("ovdje vise ne radi");
                                JSONObject pozicije = mapa.getJSONObject("fields");
                                System.out.println(kvizz);
                                try {
                                    int brojac = 1;
                                    for (; ; ) {
                                        JSONObject mapa2 = pozicije.getJSONObject(String.valueOf(brojac));
                                        JSONObject mapaa2 = mapa2.getJSONObject("mapValue");
                                        JSONObject map2Fields = mapaa2.getJSONObject("fields");
                                        String igrac = map2Fields.names().toString();
                                        igrac = igrac.replace("[", "");
                                        igrac = igrac.replace("]", "");
                                        igrac = igrac.replace("\"", "");
                                        double procenat = map2Fields.getJSONObject(igrac).getDouble("doubleValue");
                                        mapaIgracProcenat.put(igrac, procenat);
                                        //mapa sa pozicijom igraca i njegovim procentom
                                        //mapaPozicijaMapa.put(brojac, mapaIgracProcenat);
                                        //elementiListe.add(brojac + ". " + igrac + " " + procenat + "%");
                                        ++brojac;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {

            }
            return " ";
        }

        @Override
        protected void onPostExecute(String result) {
            //if (this.dialog.isShowing()) { // if dialog box showing = true
            //  this.dialog.dismiss(); // dismiss it
            //}
            interfejs.krajUcitavanja(result);
        }

        public String convertStreamToString(InputStream is) {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            try {
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }
    }

    public class UpisiURangListu extends AsyncTask<String, Integer, String> {

        public UpisiURangListu(DohvatiIzBaze interfejs) {
            //this.aktivnost = aktivnost;
            this.interfejs = interfejs;
        }

        DohvatiIzBaze interfejs = null;

        //private Activity aktivnost;
        @Override
        protected String doInBackground(String... strings) {
            InputStream is = kontekst.getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));

                credentials.refreshToken();

                String TOKEN = credentials.getAccessToken();
                Log.d("TOKEN ", TOKEN);

                String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/Rangliste?documentId="+proslijedjeni.getNaziv()+"&access_token=";
                URL urlObject = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObject.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                String pozicije = "";
                int i = 0;
                int velicina_mape = mapaIgracProcenat.size();
                System.out.println(velicina_mape);
                for (Map.Entry<String, Double> mapa : mapaIgracProcenat.entrySet()) {
                    if (i < velicina_mape - 1) {
                        pozicije += "\"" + (i+1) + "\": {\"mapValue\": {\"fields\":{\"" + mapa.getKey() + "\": {\"doubleValue\":  \"" + String.valueOf(mapa.getValue()) + "\"}}}},";

                    } else {
                        //dodati kad je posljednja pozicija iz mape da ne dodaje zarez na kraju
                        pozicije += "\"" + (i+1) + "\": {\"mapValue\": {\"fields\":{\"" + mapa.getKey() + "\": {\"doubleValue\":  \"" + String.valueOf(mapa.getValue()) + "\"}}}}";

                    }
                    i++;
                }

                String dokument = "";

                dokument = "{ \"fields\": {\"lista\":{\"mapValue\":{\"fields\":{" + pozicije + "}}}," + "\"nazivKviza\":{\"stringValue\": \"" + proslijedjeni.getNaziv()+"\"}"+"}}";

                try (OutputStream os = conn.getOutputStream()) {
                    byte[] input2 = dokument.getBytes("utf-8");
                    os.write(input2, 0, input2.length);
                }

                int code2 = conn.getResponseCode();
                InputStream odgovor2 = conn.getInputStream();
                try (BufferedReader br2 = new BufferedReader(new InputStreamReader(odgovor2, "utf-8"))) {
                    StringBuilder response2 = new StringBuilder();
                    String responseLine2 = null;
                    while ((responseLine2 = br2.readLine()) != null) {
                        response2.append(responseLine2.trim());
                    }
                    Log.d("ODGOVOR", response2.toString());
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            return " ";
        }

        @Override
        protected void onPostExecute(String aVoid) {
            interfejs.krajUcitavanja(aVoid);
        }
    }

    public class IzbrisiRangListu extends AsyncTask<String, Integer, String> {

        public IzbrisiRangListu(DohvatiIzBaze interfejs) {
            //this.aktivnost = aktivnost;
            this.interfejs = interfejs;
        }

        DohvatiIzBaze interfejs = null;

        @Override
        protected String doInBackground(String... strings) {
            InputStream is = kontekst.getResources().openRawResource(R.raw.secret);
            try {
                System.out.println("u izbrisi je");
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));

                credentials.refreshToken();

                String TOKEN = credentials.getAccessToken();
                Log.d("TOKEN ", TOKEN);

                String url = "https://firestore.googleapis.com/v1/projects/rmamyapplication/databases/(default)/documents/Rangliste/" + proslijedjeni.getNaziv() + "?access_token=";
                URL urlObject = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObject.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("DELETE");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.connect();
                System.out.println(conn.getResponseMessage());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {

            interfejs.krajUcitavanja(result);


        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_igraj_kviz_akt);

        proslijedjeni = new Kviz();

        try {
            Bundle bundle = new Bundle();
            bundle = getIntent().getExtras();
            proslijedjeni = (Kviz) bundle.getSerializable("proslijedi");
            System.out.println(proslijedjeni.getNaziv());
        } catch (Exception e) {
        }

        randomPitanjaKviza = new ArrayList<>(proslijedjeni.getPitanja());
        //izbrisemo dodaj pitanje
        randomPitanjaKviza.remove(randomPitanjaKviza.size() - 1);
        Collections.shuffle(randomPitanjaKviza);
        randomPitanjaKviza.add(new Pitanje("Kviz je zavrsen!", null, new ArrayList<String>(), null));

        brojPitanja = randomPitanjaKviza.size();
        //ovaj radi
        //System.out.println(brojPitanja);
        //System.out.println(PitanjeFrag.brojPitanja);


        //postavljanje alarma

        if(proslijedjeni.getPitanja().size() > 0){ //uvijek ce biti jedno pitanje "dodaj pitanje" tako da oduzimamo 1
            int Xminuta = (int) Math.ceil((double)(proslijedjeni.getPitanja().size() - 1) / 2);
           Intent intent = new Intent(AlarmClock.ACTION_SET_TIMER);
            Xminuta *= 60;

            intent.putExtra(AlarmClock.EXTRA_LENGTH, Xminuta);
           intent.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
           timer = intent.getData();
           this.startActivity(intent);
        }



        FragmentManager fm = getSupportFragmentManager();

        PitanjeFrag pf;
        pf = new PitanjeFrag();
        Bundle pitanje = new Bundle();
        pitanje.putSerializable("pitanje", randomPitanjaKviza.get(brojPostavljenihPitanja));
        pf.setArguments(pitanje);
        fm.beginTransaction().add(R.id.pitanjePlace, pf).commit();

        InformacijeFrag inff;
        inff = new InformacijeFrag();
        if (randomPitanjaKviza.size() != 1)
            InformacijeFrag.preostala = randomPitanjaKviza.size() - 2;
        else
            InformacijeFrag.preostala = 0;
        InformacijeFrag.tacni = 0;
        InformacijeFrag.postotak = 0;
        Bundle imeKviza = new Bundle();
        String kvizz = proslijedjeni.getNaziv();
        imeKviza.putString("ime kviza", kvizz);
        inff.setArguments(imeKviza);

        fm.beginTransaction().add(R.id.informacijePlace, inff).commit();

    }


    public void metodaKojomSeVracaUAktivnost(final boolean tacnoOdgovorio) {


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FragmentManager fm = (FragmentManager) getSupportFragmentManager();

                //mijenjamo vrijednosti u fragmentu informacija
                InformacijeFrag inff;
                inff = new InformacijeFrag();
                inff.metodaKojaMijenjaVrijednostiFragmenta(tacnoOdgovorio);

                inff = new InformacijeFrag();
                Bundle imeKviza = new Bundle();
                String kvizz = proslijedjeni.getNaziv();
                imeKviza.putString("ime kviza", kvizz);
                inff.setArguments(imeKviza);
                fm.beginTransaction().replace(R.id.informacijePlace, inff).commit();


                //onda predji na sljedece
                if (brojPostavljenihPitanja != randomPitanjaKviza.size()) {
                    brojPostavljenihPitanja++;
                    PitanjeFrag pf;
                    pf = new PitanjeFrag();
                    Bundle pitanje = new Bundle();
                    pitanje.putSerializable("pitanje", randomPitanjaKviza.get(brojPostavljenihPitanja));
                    pf.setArguments(pitanje);
                    fm.beginTransaction().replace(R.id.pitanjePlace, pf).commit();
                    if (brojPostavljenihPitanja == randomPitanjaKviza.size() - 1) {
                        //ovdje je zavrsen kviz, sada treba izbaciti alert dijalog za unos imena
                        System.out.println("Otvori dijalog");
                        openDialog();

                    }


                }
                   else{

                       /*
                    InformacijeFrag infff;
                    infff = new InformacijeFrag();
                    Bundle imeeKviza = new Bundle();
                    String kvizza = proslijedjeni.getNaziv();
                    imeeKviza.putString("ime kviza", kvizza );
                    infff.setArguments(imeeKviza);
                    fm.beginTransaction().replace(R.id.informacijePlace, infff).commit();

                    PitanjeFrag pf;
                    pf = new PitanjeFrag();
                    fm.beginTransaction().replace(R.id.pitanjePlace, pf).commit();
                    */
                }
            }
        }, 2000);

    }

    public void openDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(kontekst);
        LayoutInflater inflater = LayoutInflater.from(kontekst);
        View dialogView = inflater.inflate(R.layout.alert_dijalog_unos_imena, null);
        builder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

        builder.setTitle("Ranglista");
        builder.setMessage("Unesite vase ime: ");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                //treba uraditi nesto sa edittext
                imeIgraca = edt.getText().toString();
                System.out.println(imeIgraca);
                rangLista();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void rangLista() {

        //citanje rangliste iz baze
        konektovanoNaInternet = konektovanoNaInternet();
        if(konektovanoNaInternet) {
            new UcitavanjeRangListiIzBaze(new DohvatiIzBaze() {

                @Override
                public void krajUcitavanja(String output) {

                    //ucitano sve iz liste za ovaj kviz
                    //dodan ovaj novi igrac
                    mapaIgracProcenat.put(imeIgraca, InformacijeFrag.postotak);

                    //sortiranje mape u opadajuci poredak
                    //treba sortirati mapu po postotku
                    //sortirano u opadajucem poretku

                    Map<String, Double> mapaIgracProcenatSort = sortByComparator(mapaIgracProcenat);
                    printMap(mapaIgracProcenatSort);

                    //ovo je sada ucitano iz SQL baze
                    Map<String, Double> mapaIgracProcenatSQL = helper.dohvatiRangListuIzSQL();
                    //ako se prvi put igra kviz, znaci prazna je sql baza pa se u njenu mapu ucita ono sto je bilo u bazi firebase
                    if(mapaIgracProcenatSQL.isEmpty()){
                        mapaIgracProcenatSQL = mapaIgracProcenatSort;
                    }
                    //ako nije prazna dodamo u nju novog igraca
                    else mapaIgracProcenatSQL.put(imeIgraca, InformacijeFrag.postotak);
                    Map<String, Double> mapaIgracProcenatSortSQL = sortByComparator(mapaIgracProcenatSQL);

                    //sada to iz sql baze prikazati u fragmentu
                    elementiListe.clear();
                    //sortiranu mapu prikazujemo u fragmentu
                    int brojac = 1;
                    for (Map.Entry<String, Double> m : mapaIgracProcenatSortSQL.entrySet()) {
                        elementiListe.add(brojac + ". " + m.getKey() + " " + m.getValue() + "%");
                        brojac++;
                    }

                    //sada izbrisemo sve iz sql rangliste i postavimo joj ovu novu u koju smo dodali tog novog igraca
                    helper.obrisiRang();
                    helper.upisiRanglistuUSQL(proslijedjeni.getNaziv(), mapaIgracProcenatSortSQL);

                    //sada postavljamo mapu za firebase da bude ona koja je ucitana iz sql
                    mapaIgracProcenat = mapaIgracProcenatSortSQL;

                    System.out.println(mapaIgracProcenat.size());

                    FragmentManager fm = (FragmentManager) getSupportFragmentManager();
                    RangLista rl;
                    rl = new RangLista();
                    Bundle nazivKviza = new Bundle();
                    nazivKviza.putSerializable("elementi", elementiListe);
                    rl.setArguments(nazivKviza);
                    fm.beginTransaction().replace(R.id.pitanjePlace, rl).commit();
                    System.out.println("Presao na drugi fragment");


                }
            }).execute(" ");


            new IzbrisiRangListu(new DohvatiIzBaze() {
                @Override
                public void krajUcitavanja(String output) {
                    System.out.println("Sve ok");

                    new UpisiURangListu(new DohvatiIzBaze() {
                        //ovdje kad se pozove upisivanje u bazu, upisat ce se ona iz sql
                        @Override
                        public void krajUcitavanja(String output) {
                            System.out.println("Upisano ponovo");
                        }
                    }).execute(" ");
                }
            }).execute(" ");

        }else{
            //prvo ucitati iz sql

            mapaIgracProcenat.clear();
            mapaIgracProcenat = helper.dohvatiRangListuIzSQL();
            mapaIgracProcenat.put(imeIgraca, InformacijeFrag.postotak);

            Map<String, Double> mapaIgracProcenatSort = sortByComparator(mapaIgracProcenat);
            printMap(mapaIgracProcenatSort);

            elementiListe.clear();
            //sortiranu mapu prikazujemo u fragmentu
            int brojac = 1;
            for (Map.Entry<String, Double> m : mapaIgracProcenatSort.entrySet()) {
                elementiListe.add(brojac + ". " + m.getKey() + " " + m.getValue() + "%");
                brojac++;
            }

            helper.obrisiRang();
            helper.upisiRanglistuUSQL(proslijedjeni.getNaziv(),  mapaIgracProcenatSort);

            FragmentManager fm = (FragmentManager) getSupportFragmentManager();
            RangLista rl;
            rl = new RangLista();
            Bundle nazivKviza = new Bundle();
            nazivKviza.putSerializable("elementi", elementiListe);
            rl.setArguments(nazivKviza);
            fm.beginTransaction().replace(R.id.pitanjePlace, rl).commit();
            System.out.println("Presao na drugi fragment");

        }



    }

    private static Map<String, Double> sortByComparator(Map<String, Double> unsortMap)
    {

        List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>()
        {
            public int compare(Map.Entry<String, Double> o1,
                               Map.Entry<String, Double> o2)
            {

                    return o2.getValue().compareTo(o1.getValue());

            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public static void printMap(Map<String, Double> map)
    {
        for (Map.Entry<String, Double> entry : map.entrySet())
        {
            System.out.println("Key : " + entry.getKey() + " Value : "+ entry.getValue());
        }
    }

    public boolean konektovanoNaInternet() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else
            connected = false;

        return connected;
    }

    }


