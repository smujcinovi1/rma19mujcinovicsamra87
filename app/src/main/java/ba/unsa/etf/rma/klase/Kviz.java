package ba.unsa.etf.rma.klase;

import android.text.Editable;

import java.io.Serializable;
import java.util.ArrayList;

public class Kviz implements Serializable {
    private String naziv;
    private ArrayList<Pitanje> pitanja;
    private ArrayList<Pitanje> mogucaPitanja;
    private Kategorija kategorija;
    String idKviza;

    public Kviz(String naziv, ArrayList<Pitanje> pitanja, Kategorija kategorija) {
        this.naziv = naziv;
        this.pitanja = pitanja;
        this.kategorija = kategorija;

    }

    public Kviz() {
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Pitanje> getPitanja() {
        return pitanja;
    }

    public void setPitanja(ArrayList<Pitanje> pitanja) {
        this.pitanja = pitanja;
    }

    public Kategorija getKategorija() {
        return kategorija;
    }

    public void setKategorija(Kategorija kategorija) {
        this.kategorija = kategorija;
    }

    public void dodajPitanje(Pitanje x){
        pitanja.add(x);
    }

    public String getIdKviza(){return idKviza;}

    public void setIdKviza(String id){this.idKviza = id;}
}
