package ba.unsa.etf.rma.klase;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

public class InformacijeFrag extends Fragment {
    TextView nazivKviza;
    TextView brojTacnih;
    TextView brojPreostalihPitanja;
    TextView procenatTacnih;
    Button zavrsiKviz;
    public static int tacni , preostala;
    public static double postotak ;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle){
        //ovdje se dodjeljuje layout fragmentu, tj.sta ce se nalazitiunutar fragmenta
        return inflater.inflate(R.layout.fragment_informacije, container, false);
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        final IgrajKvizAkt aktivnost = (IgrajKvizAkt) getActivity();

        nazivKviza= (TextView) getView().findViewById(R.id.labela1);
        brojTacnih= (TextView) getView().findViewById(R.id.labela2);
        brojPreostalihPitanja= (TextView) getView().findViewById(R.id.labela3);
        procenatTacnih= (TextView) getView().findViewById(R.id.labela4);
        zavrsiKviz = (Button) getView().findViewById(R.id.btnKraj);

        if (getArguments().containsKey("ime kviza")) {
            nazivKviza.setText(getArguments().getString("ime kviza"));
        }

        brojTacnih.setText(String.valueOf(tacni));
        brojPreostalihPitanja.setText(String.valueOf(preostala));
        procenatTacnih.setText(String.valueOf(postotak));


            zavrsiKviz.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // System.out.println(PitanjeFrag.brojPitanja);
                    aktivnost.finish();
                }
            });
        }



    public void metodaKojaMijenjaVrijednostiFragmenta(final boolean tacnoOdgovorio){
        if(tacnoOdgovorio && preostala >= 0){
            tacni++;
            if(preostala != 0) {
                preostala--;
                //brojPitanja -1 -> zato sto imam dodatno pitanje koje se zove "Kviz je zavrsen!"
                postotak = (Math.round((double) tacni / (IgrajKvizAkt.brojPitanja - 1 - (preostala + 1)) * 100.0) / 100.0) * 100;
            }else{
                postotak = (Math.round((double) tacni / (IgrajKvizAkt.brojPitanja - 1 - (preostala )) * 100.0) / 100.0) * 100;
            }

        }else if(!tacnoOdgovorio && preostala >= 0){
            if(preostala != 0) {
                preostala--;
                postotak = (Math.round((double) tacni / (IgrajKvizAkt.brojPitanja - 1 - (preostala + 1)) * 100.0) / 100.0) * 100;
            }else{
                postotak = (Math.round((double) tacni / (IgrajKvizAkt.brojPitanja - 1 - (preostala )) * 100.0) / 100.0) * 100;
            }

        }
    }
}
