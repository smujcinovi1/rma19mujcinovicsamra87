package ba.unsa.etf.rma.klase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.api.client.util.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KategorijaDBOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "mojaSQLBaza.db";

    public static final int DATABASE_VERSION = 30;

    public static final String DATABASE_TABLE_KATEGORIJA = "Kategorije";
    public static final String KATEGORIJA_ID = "idKategorije";
    public static final String KATEGORIJA_NAZIV = "NazivKategorije";
    public static final String KATEGORIJA_IKONA = "IDIkone";

    public static final String DATABASE_TABLE_PITANJA = "Pitanja";
    public static final String PITANJE_ID = "idPitanja";
    public static final String PITANJE_NAZIV = "NazivPitanja";
    public static final String PITANJE_TACAN_ODG = "IndexTacnog";
    public static final String PITANJE_ODGOVORI = "Odgovori";

    public static final String DATABASE_TABLE_KVIZOVI = "Kvizovi";
    public static final String KVIZ_ID = "idKviza";
    public static final String KVIZ_NAZIV = "NazivKviza";
    //dovoljno je nazive pitanja a onda ih mogu pretraziti u listi pitanja i uzeti cijelo pitanje
    public static final String KVIZ_PITANJA = "PitanjaKviza";
    //ne trebaju moguca pitanja jer ce se samo igrati kviz
    //public static final String KVIZ_MOGUCA = "MogucaPitanja";
    //dovoljno je naziv kategorije a onda ih mogu pretraziti u listi kategorija i uzeti cijelu kategoriju
    public static final String KVIZ_KATEGORIJA = "Kategorija";
    //potreban mozda kasnije
    public static final String KVIZ_ID_FIREBASE = "id_firebase";


    public static final String DATABASE_TABLE_RANGLISTA = "RangLista";
    public static final String RANGLISTA_ID = "idRanglista";
    public static final String RANG_KVIZ = "NazivKviza";
    public static final String IGRAC = "Igrac";


    private static final String DATABASE_CREATE​_KAT = "create table " + DATABASE_TABLE_KATEGORIJA+ " (" + KATEGORIJA_ID + " integer primary key autoincrement, " +  KATEGORIJA_NAZIV + " text not null, " + KATEGORIJA_IKONA + " text not null);";
    private static final String DATABASE_CREATE​_PIT = "create table " + DATABASE_TABLE_PITANJA+ " (" + PITANJE_ID + " integer primary key autoincrement, " +  PITANJE_NAZIV + " text not null, " + PITANJE_TACAN_ODG + " text not null, "+ PITANJE_ODGOVORI + " text not null);";
    private static final String DATABASE_CREATE​_KVIZ = "create table " + DATABASE_TABLE_KVIZOVI+ " (" + KVIZ_ID + " integer primary key autoincrement, " +  KVIZ_NAZIV + " text not null, " + KVIZ_PITANJA + " text not null, "+ KVIZ_KATEGORIJA +" text not null, "+ KVIZ_ID_FIREBASE +" text not null);";
    private static final String DATABASE_CREATE​_RANGLISTA = "create table " + DATABASE_TABLE_RANGLISTA+ " (" + RANGLISTA_ID + " integer primary key autoincrement, " +  RANG_KVIZ + " text not null, " + IGRAC + " text not null);";


    public KategorijaDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public KategorijaDBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE​_KAT);
        db.execSQL(DATABASE_CREATE​_PIT);
        db.execSQL(DATABASE_CREATE​_KVIZ);
        db.execSQL(DATABASE_CREATE​_RANGLISTA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KATEGORIJA);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_PITANJA);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KVIZOVI);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_RANGLISTA);


        onCreate(db);
    }

    public void upisiKategorijuUSQL(Kategorija kat){
        System.out.println(kat.getNaziv());
        ContentValues upisi = new ContentValues();
        upisi.put(KATEGORIJA_NAZIV, kat.getNaziv());
        upisi.put(KATEGORIJA_IKONA, kat.getId());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(KategorijaDBOpenHelper.DATABASE_TABLE_KATEGORIJA, null, upisi);
        System.out.println("UPISANA kat: " + kat.getId());

    }

    public void upisiPitanjeUSQL(Pitanje p){
        ContentValues upisiP = new ContentValues();
        upisiP.put(PITANJE_NAZIV, p.getNaziv());

        String indexTacnog = p.getTacan();

        upisiP.put(PITANJE_TACAN_ODG, indexTacnog);

        String odgovori = "";
        for(int i =0; i < p.getOdgovori().size();i++){

            odgovori += p.getOdgovori().get(i);
            if(i != p.getOdgovori().size() -1 ){
                odgovori += ", ";
            }
        }

        System.out.println("odgovori: " + odgovori);
        upisiP.put(PITANJE_ODGOVORI, odgovori);

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(DATABASE_TABLE_PITANJA, null, upisiP);
        System.out.println("UPISANO pitanje: " + p.getNaziv());

    }

    public void upisiKvizUSQL(Kviz k){
        ContentValues upisiKv = new ContentValues();
        upisiKv.put(KVIZ_NAZIV, k.getNaziv());

        System.out.println("kada upisuje u sql broj pitanja kviza: " + k.getPitanja().size());
        /*for(int i=0;i<k.getPitanja().size();i++){
            if(k.getPitanja().get(i).getNaziv().equals("Dodaj Pitanje")){
                k.getPitanja().remove(i);
            }
        }*/
        String pitanjaKviza = "";
        for(int i =0; i < k.getPitanja().size();i++){
            pitanjaKviza += k.getPitanja().get(i).getNaziv();
            if(i != k.getPitanja().size() - 1) pitanjaKviza += ", ";
        }

        System.out.println("pitanja upisana string: " + pitanjaKviza);

        upisiKv.put(KVIZ_PITANJA, pitanjaKviza);

        upisiKv.put(KVIZ_KATEGORIJA, k.getKategorija().getNaziv());
        upisiKv.put(KVIZ_ID_FIREBASE, k.getIdKviza());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(DATABASE_TABLE_KVIZOVI, null, upisiKv);
        System.out.println("UPISAN kviiz " + k.getNaziv());

    }

    public void upisiRanglistuUSQL(String nazivKviza, Map<String, Double> mapa){
        ContentValues upisiRL = new ContentValues();
        upisiRL.put(RANG_KVIZ, nazivKviza);

        String igraciURang = "";
        int i=0;
        for(Map.Entry<String, Double> m : mapa.entrySet()){
            igraciURang += i + ". " + m.getKey() + " " + m.getValue();
            if(i != mapa.size()) igraciURang += ", ";
        }

        upisiRL.put(IGRAC, igraciURang);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(DATABASE_TABLE_RANGLISTA, null, upisiRL);
        System.out.println("UPISANA rang");

    }

    public void obrisiSve(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + DATABASE_TABLE_KATEGORIJA);
        db.execSQL("DELETE FROM " + DATABASE_TABLE_PITANJA);
        db.execSQL("DELETE FROM " + DATABASE_TABLE_KVIZOVI);

    }

    public void obrisiRang(){
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM " + DATABASE_TABLE_RANGLISTA);

    }

    public ArrayList<Kategorija> dohvatiKategorijeIzSQL(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor kursor;

        ArrayList<Kategorija> kategorije = new ArrayList<>();
        Kategorija katIzSQL = new Kategorija();

        kursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_KATEGORIJA, null);
        int id_kat = kursor.getColumnIndexOrThrow(KATEGORIJA_ID);
        int naziv_kat = kursor.getColumnIndexOrThrow(KATEGORIJA_NAZIV);
        int kat_ikona = kursor.getColumnIndexOrThrow(KATEGORIJA_IKONA);

        while(kursor.moveToNext()){
            katIzSQL = new Kategorija();
            katIzSQL.setNaziv(kursor.getString(naziv_kat));
            katIzSQL.setId(Integer.toString(kursor.getInt(kat_ikona)));
            System.out.println("kategorije procitaj iz sql: " + katIzSQL.getId());
            kategorije.add(katIzSQL);
        }

        kursor.close();
        for(Kategorija k : kategorije){
            System.out.println("indexi ikona iz baze: " + k.getId());
        }
        return kategorije;
    }

    public ArrayList<Pitanje> dohvatiPitanjaIzSQL(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor kursor;

        ArrayList<Pitanje> pitanja = new ArrayList<>();
        Pitanje pitIzSQL = new Pitanje();

        kursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_PITANJA, null);
        int id_pit = kursor.getColumnIndexOrThrow(PITANJE_ID);
        int naziv_pit = kursor.getColumnIndexOrThrow(PITANJE_NAZIV);
        int tacan_odg = kursor.getColumnIndexOrThrow(PITANJE_TACAN_ODG);
        int odgovori = kursor.getColumnIndexOrThrow(PITANJE_ODGOVORI);

        String odg = "";
        //ArrayList<String> odglista;
        while(kursor.moveToNext()){
            pitIzSQL = new Pitanje();
            pitIzSQL.setNaziv(kursor.getString(naziv_pit));
            pitIzSQL.setTacan(kursor.getString(tacan_odg));
            odg = kursor.getString(odgovori);
            String[] odgniz = odg.split(", ");
            System.out.println("odgovori na pitanja: " + odg);
            ArrayList<String> odglista = new ArrayList<>();
            for(int i=0;i<odgniz.length;i++){
                odglista.add(odgniz[i]);
            }
            pitIzSQL.setOdgovori(odglista);
            System.out.println("pitanja procitaj iz sql: " + pitIzSQL.getNaziv());

            pitanja.add(pitIzSQL);
        }

        kursor.close();
        return pitanja;
    }

    public ArrayList<Kviz> dohvatiKvizoveIzSQL(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor kursor;

        ArrayList<Kviz> kvizovi = new ArrayList<>();
        Kviz kvIzSQL = new Kviz();

        kursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_KVIZOVI, null);
        int id_kv = kursor.getColumnIndexOrThrow(KVIZ_ID);
        int naziv_kv = kursor.getColumnIndexOrThrow(KVIZ_NAZIV);
        int kv_pitanja = kursor.getColumnIndexOrThrow(KVIZ_PITANJA);
        int kv_kat = kursor.getColumnIndexOrThrow(KVIZ_KATEGORIJA);
        int kv_id_f = kursor.getColumnIndexOrThrow(KVIZ_ID_FIREBASE);

        ArrayList<Pitanje> pitanjaKvizova = dohvatiPitanjaIzSQL();
        System.out.println("velicinina pitanja iz pitanjaKvizovi " + pitanjaKvizova.size());
        ArrayList<Kategorija> kategorijeIzBaze = dohvatiKategorijeIzSQL();

        System.out.println("kategorija iz baze" + kategorijeIzBaze.get(0).getNaziv());

        ArrayList<Pitanje> pitanjeZaKviz = new ArrayList<>();
        String pitanja = "";
        while(kursor.moveToNext()){
            kvIzSQL = new Kviz();
            //pitanjeZaKviz.clear();
            pitanjeZaKviz = new ArrayList<>();
            System.out.println("sljedeci kviz");
            kvIzSQL.setNaziv(kursor.getString(naziv_kv));
            System.out.println("naziv kviza: " + kvIzSQL.getNaziv());
            String[] pitanjaString = kursor.getString(kv_pitanja).split(", ");
            ArrayList<String> listaPitanjaStringova = new ArrayList<>();
            for(int i = 0; i < pitanjaString.length; i++){
                listaPitanjaStringova.add(pitanjaString[i]);
            }

            for(int i = 0; i < pitanjaString.length; i++){
                System.out.println("pitanje iz kviza: " + pitanjaString[i]);
            }
            System.out.println("pitanja u nizu split zarez: " + pitanjaString.length);
            System.out.println("pitanja u listapitanjaStringova: " + listaPitanjaStringova.size());
            /*if(listaPitanjaStringova.size() == 1 && listaPitanjaStringova.get(0).equals("")){
                listaPitanjaStringova.clear();
            }*/


            for(int i = 0; i < pitanjaKvizova.size(); i++){
                for(int j = 0; j < listaPitanjaStringova.size(); j++){
                    if(listaPitanjaStringova.get(j).equals(pitanjaKvizova.get(i).getNaziv())){
                        System.out.println("pitanje iz pitanjaKvizova: " + pitanjaKvizova.get(i).getNaziv() + "== pitanje iz listaPitanjaStringova :" + listaPitanjaStringova.get(j) );
                        pitanjeZaKviz.add(pitanjaKvizova.get(i));
                    }
                }
            }
            pitanjeZaKviz.add(new Pitanje("Dodaj Pitanje", null, null, null));
            kvIzSQL.setPitanja(pitanjeZaKviz);
            System.out.println("broj pitanja kviza: " + kvIzSQL.getPitanja().size());

            String kat = kursor.getString(kv_kat);
            for(Kategorija k: kategorijeIzBaze){
                    if(k.getNaziv().equals(kat)){
                        kvIzSQL.setKategorija(k);
                        break;
                    }
            }

            //kvIzSQL.setIdKviza(Integer.toString(kursor.getInt(kv_id_f)));
           System.out.println("kategorija za kviz: "+kvIzSQL.getKategorija().getId());
            kvizovi.add(kvIzSQL);

        }

        kursor.close();
        return kvizovi;
    }

    //jos rang lista
    public Map<String, Double> dohvatiRangListuIzSQL(){
        Map<String, Double> ranglistaSQL = new HashMap<>();

        //igraciURang += i + ". " + m.getKey() + " " + m.getValue();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor kursor;

        kursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_RANGLISTA, null);
        int id_rl = kursor.getColumnIndexOrThrow(RANGLISTA_ID);
        int rang_kv = kursor.getColumnIndexOrThrow(RANG_KVIZ);
        int igrac_rl = kursor.getColumnIndexOrThrow(IGRAC);

        while(kursor.moveToNext()){
            String naziKviza = kursor.getString(rang_kv);
            String[] pozicijeIgraca = kursor.getString(igrac_rl).split(", ");
            //sada imam niz sa redovima u kojima su pozicija, ime igraca i procenat
            for(int i=0; i<pozicijeIgraca.length;i++){
                String imeIgraca = "";
                double procenat = 0;
                String[] razdvojiURedu = pozicijeIgraca[i].split(" ");
                imeIgraca = razdvojiURedu[1];
                System.out.println("IGRAC a"+ imeIgraca + "a");
                procenat = Double.valueOf(razdvojiURedu[2]);
                System.out.println("PROCENAT a" + procenat + "a");

                ranglistaSQL.put(imeIgraca, procenat);

            }
        }
        kursor.close();
        return ranglistaSQL;
    }

}
